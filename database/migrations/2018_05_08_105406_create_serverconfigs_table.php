<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServerconfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serverconfigs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token');
            $table->string('rs');
            $table->string('nom');
            $table->string('prenom');
            $table->string('telephon');
            $table->string('email');
            $table->string('noma');
            $table->string('prenoma');
            $table->string('telephona');
            $table->string('emaila');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serverconfigs');
    }
}
