<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password');
            $table->string('serveur');
            $table->string('nom');
            $table->string('adresse');
            $table->string('cpu');
            $table->integer('memoir');
            $table->date('datems');
            $table->string('marque');
            $table->string('type');
            $table->string('taille');
            $table->string('ar');
            $table->string('resolution');
            $table->date('sdatems');
            $table->integer('accepted');
            $table->integer('archived');
            $table->integer('group_id');
            $table->integer('schedule');
            $table->integer('smart')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
