<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\Player;
use App\Disposition;
use App\Schedule;
use App\Events\event_PlayerDataChanged;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups=Group::All();
        return view('group.index',compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $players=Player::All();
        return view('group.create',compact('players'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nom' => 'required|min:6',
        ]);
        
        $group=new Group();
        $group->name=$request->get('nom');
        $group->save();
        if($request->has('players'))
        {
            foreach($request->get('players') as $player)
            {
                $player=Player::find($player);
                $player->group_id=$group->id;
                $player->save();
            }
        }
        return redirect('groups')->with('success','Groupe creé');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function edit($id)
    {
        $group=Group::find($id);
        $players=Player::All();
        return view('group.edit',compact('group','players','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nom' => 'required|min:6',
        ]);
        $players=Player::All();
        $group=Group::find($id);
        $group->name=$request->get('nom');
        $group->save();
        $id=$group->id;
        foreach($players as $player)
        {
            $myBool=false;
            if($request->has('players'))
            {
                foreach($request->get('players') as $pla)
                {
                    $playertemp=Player::find($pla);
                    error_log($pla.' - '.$player->id);
                    if($player->id==$playertemp->id)
                    {
                        $player->group_id=$id;
                        $myBool=true;
                        break;
                    }
                }
                if($myBool==false)
                {
                    error_log($player->id);
                    $player->group_id=-1;
                }
            }
            
            $player->save();
        }
        
        return redirect('groups')->with('success','Groupe modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group=Group::find($id);
        $group->delete();
        return redirect()->route('groups.index')->with('success','Groupe supprimé');
    }

    public function disposition($id)
    {
        $group=Group::find($id);
        return view('group.disposition',compact('group','id'));
    }

    public function editSchedule($id)
    {
        $group=Group::find($id);
        $schedules=Schedule::All();
        return view('group.schedule',compact('schedules','group','id'));
    }

    public function updateDispo(Request $request,$id)
    {
        $dispos=Disposition::All();
        $players=Player::All();

        foreach($players as $player)
        {
            if($player->group_id==$id)
            {
                foreach($dispos as $dispo)
                {
                    if($dispo->player==$player->id)
                    {
                        $dispo->delete();
                    }
                }
                $dispo =new Disposition();
                $dispo->player=$player->id;
                $dispo->pos=$request->get('pos');
                $dispo->text1=$request->get('txt1');
                $dispo->text2=$request->get('txt2');
                $dispo->type=$request->get('type');
                $dispo->save();
                app('App\Http\Controllers\PlayerController')->storeChangelog($player->id);
            }
        }
        event(new event_PlayerDataChanged());
        return redirect('groups')->with('success','Disposition modifié');
    }

    public function updateSchedule(Request $request,$id)
    {
        $players=Player::All();

        foreach($players as $player)
        {
            if($player->group_id==$id)
            {
                $player->schedule=$request->get('schedule');
                $player->save();
                app('App\Http\Controllers\PlayerController')->storeChangelog($player->id);
            }
        }
        event(new event_PlayerDataChanged());
        return redirect('groups')->with('success','schedule modifié');
    }
}
