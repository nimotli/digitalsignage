<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Custom;
use App\Multimedia;
class CustomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Custom::All();
        return view('wysiwyg.index',compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $multimedias=Multimedia::All();
        return view('wysiwyg.create',compact('multimedias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nom'=>'required',
        ]);

        $custom=new Custom();
        $custom->name=$request->get('nom');
        $custom->html=$request->get('html');
        $custom->save();

        return redirect('annonces')->with('success','Annonce créé');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad=Custom::find($id);
        $multimedias=Multimedia::All();
        return view('wysiwyg.edit',compact('multimedias','ad','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nom'=>'required',
        ]);

        $custom=Custom::find($id);
        $custom->name=$request->get('nom');
        $custom->html=$request->get('html');
        $custom->save();
        error_log('was in the update method');
        return redirect('annonces')->with('success','Annonce modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad=Custom::find($id);
        $ad->delete();
        return redirect('annonces');
    }
}
