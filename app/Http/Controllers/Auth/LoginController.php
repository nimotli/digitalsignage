<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Storage;
use App\Right;
use App\Utilisateur;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(Storage::disk('local')->exists('conf.json'))
            $this->middleware('guest')->except('logout');
        else
            return redirect('/conf');
    }
    public static function getAccessRights()
    {
        //get the loggedin user
        //checks its rights
        //form them
        //when the view recieves the result it checks and show option accordinaly
        $rights=array();
        if (Auth::check())
        {
            if(Auth::user()->type == 0)
            {
                return 'admin';
            }
            else
            {
                $userid = Auth::user()->id;
                $utils=Utilisateur::All();
                $id=-1;
                foreach($utils as $ul)
                {
                    if($ul->userId==$userid)
                    {
                        $id=$ul->id;
                        break;
                    }
                }
                $rights=Right::All();
                $wantedRights=array();
                $finalData=array();
                foreach($rights as $right)
                {
                    if($right->utilisateur==$id)
                    {
                        array_push($wantedRights,$right);
                    }
                }
                return $wantedRights;
            }
        }
        return null;
    }
}
