<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sequence;
use App\Multimedia;
use App\mediaSequence;
use App\Custom;
use App\Player;
use App\Events\event_PlayerDataChanged;

use Auth;
class SequenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sequences=Sequence::All();
        return view('sequences/index',compact('sequences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id=Auth::user()->id;
        $multimedias=Multimedia::All();
        return view('sequences/create',compact('multimedias','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nom'=>'required',
        ]);

        $sequence=new Sequence();
        $sequence->nom=$request->get('nom');
        $sequence->overlay=-1;
        $sequence->save();
        $sucText=$request->get('sequenceInfo');

        
        if($sucText!='null')
        {
            $mss=explode('-',$sucText);
            $counter=0;
            foreach($mss as $ms)
            {
                $keyVal=explode(':',$ms);
                $mediaSeq=new mediaSequence();
                $mediaSeq->multimedia=$keyVal[1];
                $mediaSeq->sequence=$sequence->id;
                $mediaSeq->order=$keyVal[0];
                $mediaSeq->filename=Multimedia::find($keyVal[1])->filename;
                $mediaSeq->duree=$keyVal[2];
                $mediaSeq->save();
            }
        }
        return redirect('sequences')->with('success','Sequence creé');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aId=Auth::user()->id;
        $multimedias=Multimedia::All();
        $sequence=Sequence::find($id);
        $mediaSequences=mediaSequence::All();
        return view('sequences/edit',compact('sequence','id','aId','multimedias','mediaSequences'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'nom'=>'required',
        ]);

        $sequence=Sequence::find($id);
        $sequence->nom=$request->get('nom');
        $sequence->save();
        $mediaSequences=mediaSequence::All();
        $sucText=$request->get('sequenceInfo');

        foreach($mediaSequences as $ms)
        {
            if($ms->sequence==$id)
            {
                $ms->delete();
            }
        }
        
        if($sucText!='null')
        {
            $mss=explode('-',$sucText);
            $counter=0;
            foreach($mss as $ms)
            {
                $keyVal=explode(':',$ms);
                $mediaSeq=new mediaSequence();
                $mediaSeq->multimedia=$keyVal[1];
                $mediaSeq->sequence=$sequence->id;
                $mediaSeq->order=$keyVal[0];
                $mediaSeq->filename=Multimedia::find($keyVal[1])->filename;
                $mediaSeq->duree=$keyVal[2];
                $mediaSeq->save();
            }
        }
        return redirect('sequences')->with('success','Sequence modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sequence=Sequence::find($id);
        $mediaSequences=mediaSequence::All();
        foreach($mediaSequences as $ms)
        {
            if($ms->sequence==$id)
            {
                $ms->delete();
            }
        }
        $sequence->delete();
        return redirect('sequences')->with('success','Sequence supprimé');
    }

    public function overlay($id)
    {
        $ads=Custom::All();
        $sequence=Sequence::find($id);
        return view('sequences.overlay',compact('id','ads','sequence'));
    }

    public function changeOverlay(Request $request,$id)
    {
        $sequence = Sequence::find($id);
        $sequence->overlay=$request->get('overlay');
        $sequence->save();
        $players=Player::All();
        foreach($players as $player)
        {
            app('App\Http\Controllers\PlayerController')->storeChangelog($player->id);
        }
        event(new event_PlayerDataChanged());
        return redirect('sequences')->with('success','l\'élement de la supperposition a été modifié ');
    }
}
