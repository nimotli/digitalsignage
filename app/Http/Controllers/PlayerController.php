<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Player;
use App\Sequence;
use App\Schedule;
use App\mediaSequence;
use App\Disposition;
use App\Serverconfig;
use App\SchedulePlanning;
use App\Custom;
use App\Utilisateur;
use App\Right;
use App\dispositionpart;
use App\Multimedia;
use App\Playerlog;
use App\Changelog;
use App\AudienceData;
use App\Events\event_PlayerDataChanged;
use Cookie;
use Illuminate\Support\Facades\Hash;
use Storage;
use Symfony\Component\Process\Process;
class PlayerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(!Storage::disk('local')->exists('conf.json'))
            return redirect('/conf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $players=Player::All();
        return view('player.index',compact('players'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('player.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'date'=>'required',
            'dated'=>'required',
        ]);

        $player=new Player();
        $player->serveur='';
        $player->nom=$request->get('nom');
        $player->adresse=$request->get('adresse');
        $player->cpu=$request->get('cpu');
        $player->memoir=$request->get('memoir');
        $player->datems=$request->get('date');
        $player->marque=$request->get('marque');
        $player->type=$request->get('type');
        $player->taille=$request->get('taille');
        $player->ar=$request->get('ar');
        $player->resolution=$request->get('resolution');
        $player->sdatems=$request->get('dated');
        $player->accepted=0;
        $player->archived=0;
        $player->group_id=-1;
        $player->username="";
        $player->password="";
        $player->schedule=-1;
        $player->save();
        $dispo =new Disposition();
        $dispo->player=$player->id;
        $dispo->type=0;
        $dispo->save();
        return redirect('/player/login')->with('success','Player configuré');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $player=Player::find($id);
        return view('player.edit',compact('player','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nom'=>'required',
            'date'=>'required',
            'dated'=>'required',
        ]);

        $player=Player::find($id);
        $player->nom=$request->get('nom');
        $player->adresse=$request->get('adresse');
        $player->cpu=$request->get('cpu');
        $player->memoir=$request->get('memoir');
        $player->datems=$request->get('date');
        $player->marque=$request->get('marque');
        $player->type=$request->get('type');
        $player->taille=$request->get('taille');
        $player->ar=$request->get('ar');
        $player->resolution=$request->get('resolution');
        $player->sdatems=$request->get('dated');
        $player->save();

        return redirect('/player')->with('success','player modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function configure(Request $request)
    {
        $player=new Player();
        $player->serveur='';
        $player->nom=$request->get('nom');
        $player->adresse=$request->get('adresse');
        $player->cpu=$request->get('cpu');
        $player->memoir=$request->get('memoir');
        $player->datems=$request->get('date');
        $player->marque=$request->get('marque');
        $player->type=$request->get('type');
        $player->taille=$request->get('taille');
        $player->ar=$request->get('ar');
        $player->resolution=$request->get('resolution');
        $player->sdatems=$request->get('dated');
        $player->accepted=0;
        $player->archived=0;
        $player->group_id=-1;
        $player->schedule=-1;
        $player->username="";
        $player->password="";
        $player->save();
        $dispo =new Disposition();
        $dispo->player=$player->id;
        $dispo->type=0;
        $dispo->save();
        $server = Serverconfig::find(1);
        $data = array('id'=>$player->id,'serverToken'=>$server->token);
        setcookie('playerInstance', json_encode($data), time() + 8640000,'/');
        return redirect('/player/login');
    }

    public function accept($id)
    {
        $player=Player::find($id);

        $player->accepted=1;
        $player->archived=0;

        $player->save();
        return redirect('player')->with('success','Player accepté');
    }

    public function archive($id)
    {
        $player=Player::find($id);

        $player->accepted=0;
        $player->archived=1;

        $player->save();
        return redirect('player')->with('success','Player archivé');
    }

    public function dissociate($id)
    {
        $player=Player::find($id);

        $player->accepted=0;
        $player->archived=1;

        $player->save();
        return redirect('player')->with('success','Player dissocié');
    }
    public function login(Request $request)
    {
        if (isset($_COOKIE["playerInstance"]))
        {
            if(isset(json_decode($_COOKIE["playerInstance"])->id))
            {
                $data=json_decode($_COOKIE["playerInstance"]);
                $player=Player::find($data->id);
                $server = Serverconfig::find(1);
                if($data->serverToken==$server->token)
                {
                    if($player->accepted==1)
                        return redirect('player/home');
                    else
                        return redirect('player/login')->withErrors(['Player not approuved yet', 'Player not approuved yet']);
                }
                else
                    return redirect('player/configure');
            }
            else
                return redirect('player/configure');
        }
        else
            return redirect('player/configure');
    }

    public function logout(Request $request)
    {
        setcookie('playerInstance',null,-1,'/');
        //$request->session()->forget('loggedinPlayer');
        return redirect('player/login');
    }

    public function home(Request $request)
    {
        $publicPath=public_path();
        if (isset($_COOKIE["playerInstance"]))
        {
            if(isset(json_decode($_COOKIE["playerInstance"])->id))
            {
                $data=json_decode($_COOKIE["playerInstance"]);
                $player=Player::find($data->id);
                $playerId=$data->id;
                $server = Serverconfig::find(1);
                if($data->serverToken==$server->token)
                {
                    if($player->accepted==1)
                    {
                        $allowedSequences=$this->getAllowedSequences($player->id);
                        $allMs=mediaSequence::All();

                        //$player=Player::find($_COOKIE["playerInstance"]);
                        $playerDisps=Disposition::All();
                        $playerDisp=null;
                        foreach($playerDisps as $pd)
                        {
                            if($pd->player==$player->id)
                                $playerDisp=$pd;
                        }
                        $data=$this->getPlayerData($player->id);
                        $log=new Playerlog();
                        $log->player=$player->nom;
                        $log->login= date('Y-m-d H:i:s');
                        $log->ip=$this->getUserIP();
                        $log->location=$this->getLocation($log->ip);
                        $log->save();
                        //return view('player.layouttest',compact('data','allowedSequences','allMs','player','playerId','publicPath'));
                        return view('player.layouttest',compact('data','playerId'));

                    }
                    else
                        return redirect('player/login')->withErrors(['Player not approuved yet', 'Player not approuved yet']);
                }
                else
                {
                    return redirect('player/configure');
                }
            }
            else
            {
                return redirect('player/configure');
            }
        }
        else
        {
            return redirect('player/configure');
        }
    }

    function getAllowedSequences($idPlayer)
    {
        $player=Player::find($idPlayer);
        $schedule=Schedule::find($player->schedule);
        $sps=SchedulePlanning::All();
        $allowedSequences=array();
        if(isset($schedule))
        {
            foreach($sps as $sp)
            {
                $now = date("Y-m-d");
                $otherDate = $sp->scheduleDate;
                $cur=date('Y',strtotime($now)).'-'.date('n',strtotime($now)).'-'.date('j',strtotime($now));
                $scd=date('Y',strtotime($otherDate)).'-'.date('n',strtotime($otherDate)).'-'.date('j',strtotime($otherDate));
                
    
                if($sp->schedule==$schedule->id && $cur==$scd)
                {
                    $sequences=$this->getSequencesFromDataString($sp->dataString);
                    $allowedSequences=$sequences;
                }
            }
        }
        return $allowedSequences;
    }

    function getSequencesFromDataString($dataString)
    {
        $houreSequence=array();
        $data=explode('-',$dataString);
        foreach($data as $hs)
        {
            if($hs!="")
            {
                $houre=explode(':',$hs)[0];
                $convertedHoure=$this->convertTime(date('H'),date('i'));
                if($houre==0)
                {
                    $seqs=explode(',',explode(':',$hs)[1]);
                    foreach($seqs as $seq)
                    {
                        $sequenceee=Sequence::find($seq);
                        if($sequenceee->overlay>-1)
                            $tempArray=array($sequenceee,Custom::find($sequenceee->overlay));
                        else
                            $tempArray=array($sequenceee,null);
                        if(!in_array($tempArray,$houreSequence))
                            array_push($houreSequence,$tempArray);
                    }
                }
                else
                {
                    if($houre==$convertedHoure+1)
                    {
                        $seqs=explode(',',explode(':',$hs)[1]);
                        foreach($seqs as $seq)
                        {
                            $sequenceee=Sequence::find($seq);
                            if($sequenceee->overlay>-1)
                                $tempArray=array($sequenceee,Custom::find($sequenceee->overlay));
                            else
                                $tempArray=array($sequenceee,null);
                            if(!in_array($tempArray,$houreSequence))
                                array_push($houreSequence,$tempArray);
                        }
                    }
                }
            }
        }
        return $houreSequence;
    }
    function convertTime($houre,$min)
    {
        $time=0;
        if($min>=30)
            $time=$houre*2+1;
        else
            $time=$houre*2;
        return $time;
    }
    public function disposition($id)
    {
        $player=Player::find($id);
        $dispos=Disposition::All();
        $utilisateurs=Utilisateur::All();
        $dis=null;
        foreach($dispos as $dispo)
        {
            if($dispo->player==$id)
            {
                $dis=$dispo;
            }
        }
        return view('player.disposition',compact('player','id','dis','utilisateurs'));
    }
    public function updateDispo(Request $request,$id)
    {
        $dispos=Disposition::All();
        $usNames=['txttl','txttr','txtbl','txtbr'];
        foreach($dispos as $dispo)
        {
            if($dispo->player==$id)
            {
                $dispo->delete();
            }
        }
        $dispo =new Disposition();
        $dispo->player=$id;
        $dispo->pos=$request->get('pos');
        $dispo->text1=$request->get('txt1');
        $dispo->text2=$request->get('txt2');
        $dispo->type=$request->get('type');
        $dispo->save();
        if($dispo->type==4)
        {
            $rights=Right::All();

            foreach($rights as $right)
            {
                if($right->player==$id)
                    $right->delete();
            }
            for($i=0;$i<4;$i++)
            {
                $right=new Right();
                $right->utilisateur=$request->get($usNames[$i]);
                $right->name="part";
                $right->player=$id;
                $right->part=$i;
                $right->save();
                $dp=new dispositionpart();
                $dp->utilisateur=$request->get($usNames[$i]);
                $dp->part=$i;
                $dp->player=$id;
                $dp->save();
            }
        }
        event(new event_PlayerDataChanged());
        $this->storeChangelog($id);
        return redirect('player')->with('success','Disposition modifié');
    }

    public function manageConf()
    {
        if (!isset($_COOKIE["playerInstance"]))
        {
                return view('player.configure');
        }
        else
        {
            $data=json_decode($_COOKIE["playerInstance"]);
            if(isset($data->id))
            {
                $player=Player::find($data->id);
                $server = Serverconfig::find(1);
                if($data->serverToken==$server->token)
                {
                    return redirect('player/login');
                }
                else
                    return view('player.configure');
            }
            else
                return view('player.configure');
        }
    }

    public function schedule($id)
    {
        $schedules=Schedule::All();
        $player=Player::find($id);
        return view('player.setschedule',compact('player','id','schedules'));
    }

    public function editSchedule($id)
    {
        $schedules=Schedule::All();
        $player=Player::find($id);
        return view('player.updateschedule',compact('player','id','schedules'));
    }

    public function updateSchedule(Request $request,$id)
    {
        $player=Player::find($id);
        $player->schedule=$request->get('schedule');
        $player->save();
        event(new event_PlayerDataChanged());
        $this->storeChangelog($id);
        return redirect('player')->with('success','Planning modifié');
    }

    public function partindex()
    {
        $userid = Auth::user()->id;
        $utils=Utilisateur::All();
        $id=-1;
        $dparts=array();
        $parts=['Top left','Top right','Bottom left','Bottom right'];
        foreach($utils as $ul)
        {
            if($ul->userId==$userid)
            {
                $id=$ul->id;
                break;
            }
        }
        $alldp=dispositionpart::All();
        foreach($alldp as $dp)
        {
            if(Utilisateur::find($dp->utilisateur)->userId==$userid )
            {
                array_push($dparts,$dp);
            }
        }

        $data=array();
        foreach($dparts as $dp)
        {
            $temp=array('id'=>$dp->id,'player'=>Player::find($dp->player)->nom,'part'=>$parts[$dp->part]);
            array_push($data,$temp);
        }
        return view('segment.index',compact('data'));
    }

    public function editPart($id)
    {
        $multimedias=Multimedia::All();
        $dp=dispositionpart::find($id);
        $aId=Auth::user()->id;
        return view('segment.edit',compact('multimedias','aId','id','dp'));
    }

    public function updatePart(Request $request,$id)
    {
        $dp=dispositionpart::find($id);
        $dp->multimedia=$request->get('multimedia');
        $dp->save();
        $this->storeChangelog($dp->player);
        event(new event_PlayerDataChanged());
        return redirect('/part')->with('success','Partie modifié');
    }
    public static function getplayerMult($id)
    {
        $dps =dispositionpart::All();
        $cdps=array();
        $mults=array();
        foreach($dps as $dp)
        {
            if($dp->player==$id)
            {
                array_push($mults,Multimedia::find($dp->multimedia));
            }
        }
        return $mults;
    }

    public function archivedIndex()
    {
        $players=Player::All();
        return view('player.archivedindex',compact('players'));
    }
    function getUserIP()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
    
        return $ipaddress;
    }
    function getLocation($ip)
    {
        $key='64e4705aeded84509f5b058ae3b71a3b938cc51b1510d7597d4f78b134d889ce';
        $googleKey='AIzaSyAyeM2NxvDSGm9FlqFESi_t2KKz8bPweGY';
        if(explode('.',$ip)[0]==127)
            $url='http://api.ipinfodb.com/v3/ip-city/?key='.$key.'&format=json';
        else
            $url='http://api.ipinfodb.com/v3/ip-city/?key='.$key.'&ip='.$ip.'&format=json';
        $du = file_get_contents($url);
        $djd = json_decode(utf8_encode($du),true);
        $latlon=$djd['latitude'].','.$djd['longitude'];
        $googleUrl='https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAyeM2NxvDSGm9FlqFESi_t2KKz8bPweGY&latlng='.$latlon.'&sensor=false&language=fr%27';
        $locationRaw = file_get_contents($googleUrl);
        $locationJson = json_decode(utf8_encode($locationRaw),true);  
        if(isset($locationJson['results'][0]['formatted_address']))
            return $locationJson['results'][0]['formatted_address'];
        return null;
    }
    
    function storeChangelog($player)
    {
        $changelog=new Changelog();
        $changelog->player=$player;
        $changelog->wasread=0;
        $changelog->save();
    }

    public function checkIfChanged(Request $request)
    {
        $id = $request->get('player');
        $logs=Changelog::All();
        $hasChanged="false";
        foreach($logs as $log)
        {
            if($log->wasread==0)
            {
                if($log->player==$id)
                {
                    $hasChanged="true";
                    $log->wasread=1;
                    $log->save();
                }
            }
        } 
        return $hasChanged;
    }

    public function getPlayerData($player)
    {
        $player=Player::find($player);
        $allowedSequences=$this->getAllowedSequences($player->id);
        $allMs=mediaSequence::All();
        $playerDisps=Disposition::All();
        $playerDisp=null;
        foreach($playerDisps as $pd)
        {
            if($pd->player==$player->id)
                $playerDisp=$pd;
        }

        $data=$this->formatPlayerData($player,$player->id,$allowedSequences,$allMs,$playerDisp);

        return $data;
    }
    public function SendPlayerDataAjax(Request $request)
    {
        
        $player=Player::find($request->get('player'));

        $allowedSequences=$this->getAllowedSequences($player->id);
        $allMs=mediaSequence::All();
        $playerDisps=Disposition::All();
        $playerDisp=null;
        foreach($playerDisps as $pd)
        {
            if($pd->player==$player->id)
                $playerDisp=$pd;
        }

        $data=$this->formatPlayerData($player,$player->id,$allowedSequences,$allMs,$playerDisp);

        return $data;
    }

    function formatPlayerData($player,$playerId,$allowedSequences,$allMs,$playerDisp)
    {

        //instead of echo put em in a var that will be returned 
        //create dynamic css elements that change depending on display ..etc
        $normalDisp=true;
        $topTextBar=false;
        $botTextBar=false;
        $both=false;
        $htmlData=array();

        if($playerDisp->type==2)
        {
            $topTextBar=false;
            $botTextBar=false;
            $both=true;
            $htmlData=['height'=>86,'topText'=>$playerDisp->text1,'botText'=>$playerDisp->text2,'top'=>7,'bot'=>7];
        }
        else if($playerDisp->type==1 && $playerDisp->pos==0)
        {
            $topTextBar=true;
            $botTextBar=false;
            $both=false;
            $htmlData=['height'=>93,'topText'=>$playerDisp->text1,'botText'=>"",'top'=>7,'bot'=>0];
        }
        else if($playerDisp->type==1 && $playerDisp->pos==1)
        {
            $topTextBar=false;
            $botTextBar=true;
            $both=false;
            $htmlData=['height'=>93,'topText'=>"",'botText'=>$playerDisp->text1,'top'=>0,'bot'=>7];
        }
        else if($playerDisp->type==0)
        {
            $normalDisp=true;
            $topTextBar=false;
            $botTextBar=false;
            $both=false;
            $htmlData=['height'=>100,'topText'=>"",'botText'=>"",'top'=>0,'bot'=>0];
        }
        else
        {
            $normalDisp=false;
        }



        $data="";
        if($player->smart == 1)
        {
            $data='<p style="display:none" id="pageConfig">smart</p>';
        }
        else
        {
            $data='<p style="display:none" id="pageConfig">normal</p>';}
        if($normalDisp)
        {
            $data.= '<div style="height:100vh;">';
            if($topTextBar || $both)
            {
                $data.='<div style="position:relative;background-color:black;height:7vh;width:100%;z-index:5000;">'.
                '<marquee behavior="scroll" direction="left"><h4 style="color:white;">'.$htmlData['topText'].'</h4></marquee>'.
                '</div>';
            }
            
            $data.='<div  style="position:relative;height:'.$htmlData['height'].'vh;width:100%;overflow:hidden;">';
            foreach($allowedSequences as $sequence)
            {
                if($sequence[1]!=null)
                {
                    $data.='<div class="overlays" id="overlay'.$sequence[1]->id.'" style="display:none">'. $sequence[1]->html .'</div>';
                }
            }
            $data.='<ul id="slides" style="position:absolute; height:100%;width:100%;background-color:black;">';
            foreach($allowedSequences as $sequence)
            {
                foreach($allMs as $ms)
                {
                    if($ms->sequence==$sequence[0]->id)
                    {
                        if(explode('.',$ms->filename)[1]=="mp4" || explode('.',$ms->filename)[1]=="3gp"|| explode('.',$ms->filename)[1]=="mov"|| explode('.',$ms->filename)[1]=="wmv"|| explode('.',$ms->filename)[1]=="flv"|| explode('.',$ms->filename)[1]=="avi")
                        {
                            $data.= '<li style="display:none;position:fixed;top:'.$htmlData['top'].'vh;bot:'.$htmlData['bot'].'vh;width:100%;">'.
                                '<video src="'. asset("multimedia/".$ms->filename).'" style="min-height:'.$htmlData['height'].'vh;min-width:100%;" autoplay loop></video>'.
                                '<p style="diplay:none;">'.$ms->duree.'</p>'.
                                '<p style="diplay:none;">'.$sequence[0]->overlay.'</p>'.
                            '</li>';
                        }
                        else
                        {
                            $data.='<li style="display:none;position:fixed;top:'.$htmlData['top'].'vh;bot:'.$htmlData['bot'].'vh;width:100%;">
                                <img src="'.asset("multimedia/".$ms->filename).'" style="height:'.$htmlData['height'].'vh;width:100%;">
                                <p style="diplay:none;">'.$ms->duree.'</p>
                                <p style="diplay:none;">'.$sequence[0]->overlay.'</p>
                            </li>';
                        }
                    }
                }
            }
            $data.='</ul>
            </div>';

            if($botTextBar || $both)
            {
                $data.='<div style="position:relative;background-color:black;height:7vh;width:100%;z-index:5000;">
                <marquee behavior="scroll" direction="left"><h4 style="color:white;">'.$playerDisp->text1.'</h4></marquee>
                </div>';
            }
            $data.='</div>';
            $data.='<li id="imgOriginal" style="display:none;position:fixed;top:'.$htmlData['top'].'vh;bot:'.$htmlData['bot'].'vh;width:100%;">
                <img style="height:'.$htmlData['height'].'vh;width:100%;">
                <p style="diplay:none;">5</p>
                <p style="diplay:none;">temporary</p>
                <div class="smartData" style="padding:5px;border:1px solid grey;position:fixed;top:0;right:0;background-color:rgba(0,0,0,0.5);z-index:10000;">
                </div>
            </li>
            <li id="vidOriginal" style="display:none;position:fixed;top:'.$htmlData['top'].'vh;bot:'.$htmlData['bot'].'vh;width:100%;">'.
                '<video style="min-height:'.$htmlData['height'].'vh;min-width:100%;" autoplay loop></video>'.
                '<p style="diplay:none;">5</p>'.
                '<p style="diplay:none;">temporary</p>'.
                '<div class="smartData" style="padding:5px;border:1px solid grey;position:fixed;top:0;right:0;background-color:rgba(0,0,0,0.5);z-index:10000;">
                </div>'.
            '</li>';
        }
        else
        {
            return $this->getFourSeqData($playerId);
        }
        return $data;
    }


    function getFourSeqData($playerId)
    {
        $data="";
        $mults=$this->getplayerMult($playerId); 
        $data.='<div class="border" style="height: 100vh;width:100%;position:absolute;top:0;left:0;overflow:hidden;background-color:black;">
            <div class="row p-0 m-0" style="height:50vh;">
            <div class="col-6 p-0 border-right border-bottom">';
        if(isset($mults[0]))
        {
            if(explode('.',$mults[0]->filename)[1]=="mp4" || explode('.',$mults[0]->filename)[1]=="3gp"|| explode('.',$mults[0]->filename)[1]=="mov"|| explode('.',$mults[0]->filename)[1]=="wmv"|| explode('.',$mults[0]->filename)[1]=="flv"|| explode('.',$mults[0]->filename)[1]=="avi")
                $data.='<video src="'.asset('multimedia/'.$mults[0]->filename).'" style="height:50vh;width:100%;" autoplay loop></video>';
            else
                $data.='<img src="'. asset('multimedia/'.$mults[0]->filename).'" style="height:50vh;width:100%;">';
        }               
    
        $data.='</div> 
            <div class="col-6 p-0">';
        if(isset($mults[1]))
        {
            if(explode('.',$mults[1]->filename)[1]=="mp4" || explode('.',$mults[1]->filename)[1]=="3gp"|| explode('.',$mults[1]->filename)[1]=="mov"|| explode('.',$mults[1]->filename)[1]=="wmv"|| explode('.',$mults[1]->filename)[1]=="flv"|| explode('.',$mults[1]->filename)[1]=="avi")
            $data.='<video src="'.asset('multimedia/'.$mults[1]->filename).'" style="height:50vh;width:100%;" autoplay loop></video>';
            else
                $data.='<img src="'. asset('multimedia/'.$mults[1]->filename).'" style="height:50vh;width:100%;">';
        }
        $data.='</div>
            </div>
            <div class="row p-0" style="height:50vh;">
            <div class="col-6 p-0 m-0 border-right">';
        if(isset($mults[2]))
        {
            if(explode('.',$mults[2]->filename)[1]=="mp4" || explode('.',$mults[2]->filename)[1]=="3gp"|| explode('.',$mults[2]->filename)[1]=="mov"|| explode('.',$mults[2]->filename)[1]=="wmv"|| explode('.',$mults[2]->filename)[1]=="flv"|| explode('.',$mults[2]->filename)[1]=="avi")
                $data.='<video src="'.asset('multimedia/'.$mults[2]->filename).'" style="height:50vh;width:100%;" autoplay loop></video>';
            else
                $data.='<img src="'. asset('multimedia/'.$mults[2]->filename).'" style="height:50vh;width:100%;">';
        }
        $data.='</div> 
            <div class="col-6 p-0 border-top">';
        if(isset($mults[3]))
        {
            if(explode('.',$mults[3]->filename)[1]=="mp4" || explode('.',$mults[3]->filename)[1]=="3gp"|| explode('.',$mults[3]->filename)[1]=="mov"|| explode('.',$mults[3]->filename)[1]=="wmv"|| explode('.',$mults[3]->filename)[1]=="flv"|| explode('.',$mults[3]->filename)[1]=="avi")
                $data.='<video src="'.asset('multimedia/'.$mults[3]->filename).'" style="height:50vh;width:100%;" autoplay loop></video>';
            else
                $data.='<img src="'. asset('multimedia/'.$mults[3]->filename).'" style="height:50vh;width:100%;">';
        }
        $data.='</div>
        </div>
        </div>';
        return $data;
    }
    public function processImage(Request $request)
    {
        //error_log('this is the server ip : '.json_encode(request()->server));
        $img = $_POST['imgBase64'];
        $playerId=$_POST['player'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $imgName=uniqid() . '.jpg';
        $file = '/images/' . $imgName;
        
        Storage::disk('local')->put($file, $data);
        $tempPath=storage_path('app').'!images!'.$imgName;
        $theImage=str_replace('/','!',$tempPath);
        $theImage=str_replace('\\','!',$theImage);
        $url=$_SERVER['HTTP_HOST'].':5000/analyse/'.$theImage;
        //return $url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        $data = json_decode($result);
        curl_close($ch);      
        Storage::disk('local')->delete($file);
        $mediasStrenght=array();
        $mlts=Multimedia::All();

        foreach($mlts as $multi)
        {
            $strenght=0;
            if($multi->tags!="")
            {
                $tags=explode(',',$multi->tags);
                foreach($tags as $tag)
                {
                    if($tag != "")
                    {
                        foreach($data as $face)
                        {
                            if($face->gender==$tag)
                                $strenght++;
                            if($face->emotion==$tag)
                                $strenght++;
                        }
                    }
                }
            }
            $tempM=['multimedia'=>$multi,'strenght'=>$strenght];
            array_push($mediasStrenght,$tempM);
        }
        usort($mediasStrenght, function ($a, $b){
            return strcmp($a["strenght"], $b["strenght"]);
        });
        $currentTime=date('h');
        $pd="";
        if($currentTime>=5 && $currentTime < 12)
            $pd='matin';
        else if($currentTime < 17)
            $pd='après midi';
        else if($currentTime < 21)
            $pd='soir';
        else
            $pd='nuit';
        
            

        
        foreach($data as $face)
        {
            $ad = new AudienceData();
            $ad->gender=$face->gender;
            $ad->emotion=$face->emotion;
            $ad->period=$pd;
            $ad->player=$playerId;
            $ad->save();
        }
        if(count($data)>0)
            $wantedMedia=$mediasStrenght[count($mediasStrenght)-1]['multimedia'];
        else
            return "nothingFound";
        return $wantedMedia->tags.'-'.$wantedMedia->filename.'-'.$wantedMedia->media_type.'-'.json_encode($data).'-'.public_path();
    }


    public function changeMode(Request $request)
    {
        $id=$request->get('player');
        $checked=$request->get('checked');
        $player=Player::find($id);
        if($checked=='true'){
            $player->smart=1;
        }
        if($checked == 'false'){
            $player->smart=0;
        }
        $player->save();
        event(new event_PlayerDataChanged());
        $this->storeChangelog($id);
    }
}
