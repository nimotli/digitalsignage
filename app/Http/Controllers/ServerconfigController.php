<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Serverconfig;
use App\User;
use Illuminate\Support\Facades\Hash;
use Storage;
use Artisan;
use DB;
use Config;
class ServerconfigController extends Controller
{
    public function index()
    {
        if(Storage::disk('local')->exists('conf.json'))
        {
            return redirect('/');
        }
        else
        {
            return view('ServerConf.conf');
            //redirect('utilisateurs');
        }
    }

    public function configure(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'emaila' => 'required|email',
            'token' => 'required|min:6',
            'password' => 'min:6|confirmed',
        ]);

        $dbName=env('DB_DATABASE', 'digitalsignage');

        DB::connection('mysql_only_connect')->statement('create database '.$dbName );

        Artisan::call('migrate',
        [
            '--database' => 'mysql',
            '--path'     => 'database/migrations',
            '--step'     => true,
            '--force'    => true
        ]);
        $serverconfig=new Serverconfig();
        $serverconfig->token=$request->get('token');
        $serverconfig->rs=$request->get('rs');
        $serverconfig->nom=$request->get('nom');
        $serverconfig->prenom=$request->get('prenom');
        $serverconfig->telephon=$request->get('phon');
        $serverconfig->email=$request->get('email');
        $serverconfig->noma=$request->get('noma');
        $serverconfig->prenoma=$request->get('prenoma');
        $serverconfig->telephona=$request->get('phona');
        $serverconfig->emaila=$request->get('emaila');
        $serverconfig->save();
        $adminData=array('name'=>$request->get('noma'),'email'=>$request->get('emaila'),'password'=>$request->get('password'),'type'=>'0');
        app('App\Http\Controllers\Auth\RegisterController')->create($adminData);
        $data=json_encode($serverconfig);
        Storage::disk('local')->put('conf.json', $data);
        return redirect('/dashboard')->with('success', 'Serveur configuré');


    }

    public function edit()
    {
        $sc=Serverconfig::All()[0];
        return view('parameters.edit',compact('sc'));
    }
    public function update(Request $request)
    {
        $sc=User::All()[0];

        if (!Hash::check($request->get('oldpassword'), $sc->password)) {
            return back()->withErrors(['Le mot de passe actuel est incorrect']);
        }
        else
        {
            $this->validate($request,[
                'email' => 'required|email',
                'emaila' => 'required|email',
                'token' => 'required|min:6',
                'password' => 'min:6|confirmed|different:oldpassword',
            ]);

            $serverconfig=Serverconfig::All()[0];
            $serverconfig->token=$request->get('token');
            $serverconfig->rs=$request->get('rs');
            $serverconfig->nom=$request->get('nom');
            $serverconfig->prenom=$request->get('prenom');
            $serverconfig->telephon=$request->get('phon');
            $serverconfig->email=$request->get('email');
            $serverconfig->noma=$request->get('noma');
            $serverconfig->prenoma=$request->get('prenoma');
            $serverconfig->telephona=$request->get('phona');
            $serverconfig->emaila=$request->get('emaila');
            $adminData=array('name'=>$request->get('noma'),'email'=>$request->get('emaila'),'password'=>Hash::make($request->get('password')),'type'=>'0');
            User::All()[0]->update($adminData);

            
            return redirect('/dashboard')->with('success','Parametres modifié');
        }
    }
}
