<?php

namespace App\Http\Controllers;
use Storage;
use Illuminate\Http\Request;
use App\Utilisateur;
use App\Player;
use App\Playerlog;
use App\Multimedia;
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(Storage::disk('local')->exists('conf.json'))
            $this->middleware('auth');
        else
            return redirect('/conf');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Storage::disk('local')->exists('conf.json'))
        {
            $data=$this->getDashboardData();
            $logs = Playerlog::orderBy('id', 'desc')->take(10)->get();
            return view('index',compact('data','logs'));
        }
        else
            return redirect('/conf');
    }

    public function getDashboardData()
    {
        $data=null;
        //{{ asset('multimedia/'.$mult->filename)}}

        //users
        $users=count(Utilisateur::All());
        //players
        $players=Player::All();
        
        $asso=0;$archi=0;$att=0;
        foreach($players as $player)
        {
            if($player->archive==1)
            {
                $archi++;
            }
            else
            {
                if($player->accepted!=0)
                    $asso++;
                else
                    $att++;
            }
        }
        $playerArray=array('associated'=>$asso,'archived'=>$archi,'waiting'=>$att);
        
        //multimedia
        $multimedias=Multimedia::All();
        $countMult=count($multimedias);
        $sizeMult=0;
        foreach($multimedias as $multi)
        {
            $sizeMult+=filesize(public_path().'/multimedia/'.$multi->filename);
        }
        $sizeMult=ceil($sizeMult/1048576);
        $data=array('users'=>$users,'playerCount'=>count($players),'playerPie'=>$playerArray,'multCount'=>$countMult,'multSize'=>$sizeMult);
        return $data;
    }


}
