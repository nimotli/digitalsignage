<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use App\Sequence;
use App\SchedulePlanning;
use DB;
class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules=Schedule::All();
        $sp=SchedulePlanning::All();
        return view('schedules.index',compact('schedules','sp'));
    }
    public function createSchedule($id)
    {
        $content=$this->draw_calendar(date('n',strtotime("now")),date('Y',strtotime("now")));
        $sequences=Sequence::All();
        return view('schedules.createSchedule',compact('content','sequences','id'));
    }
    public function editSchedule($id)
    {
        //$content=$this->draw_calendar(date('n',strtotime("now")),date('Y',strtotime("now")));
        $sequences=Sequence::All();
        $sps = DB::select('select * from schedule_plannings where schedule = ?', [$id]);

        $dataString="";
        $counter=0;
        $months=array();
        $setDays=array();
        foreach($sps as $sp)
        {
            array_push($setDays,$sp->scheduleDate);
            $monthYear=date('n',strtotime($sp->scheduleDate)).'-'.date('Y',strtotime($sp->scheduleDate));
            if(!in_array($monthYear,$months))
                array_push($months,$monthYear);
            if($counter < count($sps)-1)
                $dataString.=$sp->scheduleDate.'!'.$sp->dataString.'|';
            else
                $dataString.=$sp->scheduleDate.'!'.$sp->dataString;
            $counter++;
        }
        $content=$this->draw_edit_calendar($months,$setDays);
        return view('schedules.editSchedule',compact('content','sequences','id','dataString'));
    }
 
    public function sendNewCalendar(Request $request)
    {
        $month=$request->get('month');
        $year=$request->get('year');

        $content=$this->draw_calendar($month,$year);
        return response($content)->header('Content-Type', 'text/plain');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schedules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nom'=>'required',
        ]);

        $schedule=new Schedule();
        $schedule->name=$request->get('nom');
        $schedule->save();
        return redirect('schedules')->with('success','Planning ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule=Schedule::find($id);

        return view('schedules.edit',compact('schedule','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nom'=>'required',
        ]);

        $schedule= Schedule::find($id);
        $schedule->name=$request->get('nom');
        $schedule->save();
        return redirect('schedules')->with('success','Planning modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule=Schedule::find($id);
        
        $sps=SchedulePlanning::All();

        foreach($sps as $sp)
        {
            if($sp->schedule==$id)
                $sp->delete();
        }
        $schedule->delete();
        return redirect('schedules')->with('success','schedule supprimé');
    }

    public function makeSchedule(Request $request,$id)
    {
        $rowDataString=$request->get('dataString');
        $days=explode('|',$rowDataString);
        $date="";
        foreach($days as $day)
        {
            if($day!="")
            {
                $dataString="";
                $dateHoures=explode('!',$day);
                $date=$dateHoures[0];
                $houreSequence=explode('-',$dateHoures[1]);
                
                foreach($houreSequence as $hs)
                {
                    $houCounter=0;
                    if($hs!="")
                    {
                        $temphs=explode(':',$hs);
                        $houre=$temphs[0];
                        $sequences=explode(',',$temphs[1]);
                        $dataString.=$houre.':';
                        $secCounter=0;
                        foreach($sequences as $sequence)
                        {
                            if($sequence!="")
                            {
                                if($secCounter==0)
                                {
                                    $secCounter++;
                                    $dataString.=$sequence;
                                }
                                else
                                    $dataString.=','.$sequence;
                            }
                        }
                        if($houCounter==0)
                        {
                            $dataString.='-';
                            $houCounter++;
                        }
                    }
                }

                $sp=new SchedulePlanning();
                $sp->schedule=$id;
                $sp->scheduleDate=$date;
                $sp->dataString=$dataString;
                $sp->save();
            }
        }
        return redirect('schedules')->with('success','schedule was set');
    }

    public function updateSchedule(Request $request,$id)
    {
        $sps=SchedulePlanning::All();

        foreach($sps as $sp)
        {
            if($sp->schedule==$id)
                $sp->delete();
        }

        $rowDataString=$request->get('dataString');
        $days=explode('|',$rowDataString);
        $date="";
        foreach($days as $day)
        {
            if($day!="")
            {
                $dataString="";
                $dateHoures=explode('!',$day);
                $date=$dateHoures[0];
                $houreSequence=explode('-',$dateHoures[1]);
                
                foreach($houreSequence as $hs)
                {
                    $houCounter=0;
                    if($hs!="")
                    {
                        $temphs=explode(':',$hs);
                        $houre=$temphs[0];
                        $sequences=explode(',',$temphs[1]);
                        $dataString.=$houre.':';
                        $secCounter=0;
                        foreach($sequences as $sequence)
                        {
                            if($sequence!="")
                            {
                                if($secCounter==0)
                                {
                                    $secCounter++;
                                    $dataString.=$sequence;
                                }
                                else
                                    $dataString.=','.$sequence;
                            }
                        }
                        if($houCounter==0)
                        {
                            $dataString.='-';
                            $houCounter++;
                        }
                    }
                }

                $sp=new SchedulePlanning();
                $sp->schedule=$id;
                $sp->scheduleDate=$date;
                $sp->dataString=$dataString;
                $sp->save();
            }
        }
        return redirect('schedules')->with('success','schedule was reset');
    }

    function draw_calendar($month,$year){

        $months=array('Janvier','Février','Mars','Avril','May','Juin','Juillet','Août','Septembre','Octobre','Novombre','Decembre');
        /* draw table */
        $calendar='<div class="calDiv"><center><h3>'.$months[$month-1].'-'.$year.'</h3></center>';
        $calendar .= '<table cellpadding="0" cellspacing="0" class="calendar" id="'.$month.'-'.$year.'">';
    
        /* table headings */
        $headings = array('Dimanch','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi');
        $calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';
    
        /* days and weeks vars now ... */
        $running_day = date('w',mktime(0,0,0,$month,1,$year));
        $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
        $days_in_this_week = 1;
        $day_counter = 0;
        $dates_array = array();

        /* row for week one */
        $calendar.= '<tr class="calendar-row">';
    
        /* print "blank" days until the first of the current week */
        for($x = 0; $x < $running_day; $x++):
            $calendar.= '<td class="calendar-day-np"> </td>';
            $days_in_this_week++;
        endfor;
    
        /* keep going with days.... */
        for($list_day = 1; $list_day <= $days_in_month; $list_day++):
            
            
                /** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
                $schedules=Schedule::All();
                $found=false;
                foreach($schedules as $schedule)
                {
                    if(date('n',strtotime($schedule->scheduleDate))==$month &&
                    date('Y',strtotime($schedule->scheduleDate))==$year &&
                    date('j',strtotime($schedule->scheduleDate))==$list_day)
                    {
                        $found=true;
                        break;
                    }
                }
                if($found)
                {
                    $calendar.= '<td class="calendar-day" id="'.$year.'-'.$month.'-'.$list_day.'" >';
                    /* add in the day number */
                    $calendar.= '<div class="day-number  bg-warning">'.$list_day.'</div>';
                }
                else
                {
                    $calendar.= '<td class="calendar-day" id="'.$year.'-'.$month.'-'.$list_day.'">';
                    /* add in the day number */
                    $calendar.= '<div class="day-number">'.$list_day.'</div>';
                }
                
            $calendar.= '</td>';
            if($running_day == 6):
                $calendar.= '</tr>';
                if(($day_counter+1) != $days_in_month):
                    $calendar.= '<tr class="calendar-row">';
                endif;
                $running_day = -1;
                $days_in_this_week = 0;
            endif;
            $days_in_this_week++; $running_day++; $day_counter++;
        endfor;
    
        /* finish the rest of the days in the week */
        if($days_in_this_week < 8):
            for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                $calendar.= '<td class="calendar-day-np"> </td>';
            endfor;
        endif;
    
        /* final row */
        $calendar.= '</tr>';
    
        /* end the table */
        $calendar.= '</table></div>';
        
        /* all done, return result */
        return $calendar;
    }
    function draw_edit_calendar($monthss,$setDays){

        $counter=0;
        $months=array('Janvier','Février','Mars','Avril','May','Juin','Juillet','Août','Septembre','Octobre','Novombre','Decembre');
        $calendar="";
        foreach($monthss as $monthYear)
        {
            $month=explode('-',$monthYear)[0];
            $year=explode('-',$monthYear)[1];
            /* draw table */
            if($counter==0)
            {
                $calendar.='<div class="calDiv" ><center><h3>'.$months[$month-1].'-'.$year.'</h3></center>';
                $counter++;
            }
            else
            {
                $calendar.='<div class="calDiv" style="display:none;"><center><h3>'.$months[$month-1].'-'.$year.'</h3></center>';
            }
            $calendar .= '<table cellpadding="0" cellspacing="0" class="calendar" id="'.$month.'-'.$year.'">';
        
            /* table headings */
            $headings = array('Dimanch','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi');
            $calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';
        
            /* days and weeks vars now ... */
            $running_day = date('w',mktime(0,0,0,$month,1,$year));
            $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
            $days_in_this_week = 1;
            $day_counter = 0;
            $dates_array = array();

            /* row for week one */
            $calendar.= '<tr class="calendar-row">';
        
            /* print "blank" days until the first of the current week */
            for($x = 0; $x < $running_day; $x++):
                $calendar.= '<td class="calendar-day-np"> </td>';
                $days_in_this_week++;
            endfor;
        
            /* keep going with days.... */
            for($list_day = 1; $list_day <= $days_in_month; $list_day++):
                
                
                    /** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
                    $schedules=$setDays;
                    $found=false;
                    foreach($schedules as $schedule)
                    {
                        if(date('n',strtotime($schedule))==$month &&
                        date('Y',strtotime($schedule))==$year &&
                        date('j',strtotime($schedule))==$list_day)
                        {
                            $found=true;
                            break;
                        }
                    }
                    if($found)
                    {
                        $calendar.= '<td class="calendar-day" id="'.$year.'-'.$month.'-'.$list_day.'" >';
                        /* add in the day number */
                        $calendar.= '<div class="day-number  bg-warning">'.$list_day.'</div>';
                    }
                    else
                    {
                        $calendar.= '<td class="calendar-day" id="'.$year.'-'.$month.'-'.$list_day.'">';
                        /* add in the day number */
                        $calendar.= '<div class="day-number">'.$list_day.'</div>';
                    }
                    
                $calendar.= '</td>';
                if($running_day == 6):
                    $calendar.= '</tr>';
                    if(($day_counter+1) != $days_in_month):
                        $calendar.= '<tr class="calendar-row">';
                    endif;
                    $running_day = -1;
                    $days_in_this_week = 0;
                endif;
                $days_in_this_week++; $running_day++; $day_counter++;
            endfor;
        
            /* finish the rest of the days in the week */
            if($days_in_this_week < 8):
                for($x = 1; $x <= (8 - $days_in_this_week); $x++):
                    $calendar.= '<td class="calendar-day-np"> </td>';
                endfor;
            endif;
        
            /* final row */
            $calendar.= '</tr>';
        
            /* end the table */
            $calendar.= '</table></div>';
            
            /* all done, return result */
        }
        
        return $calendar;
    }

}
