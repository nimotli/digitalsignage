<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Multimedia;
use Auth;
use File;
class MultimediaController extends Controller
{

    private $photos_path;

    public function __construct()
    {
        $this->photos_path = public_path('/multimedia');
    }

    public function index()
    {
        $multimedias=Multimedia::All();
        $id=Auth::user()->id;
        return view('multimedia.index',compact('multimedias','id'));
    }
    public function create()
    {
        return view('multimedia.upload');
    }
    public function store(Request $request)
    {
        $photos = $request->file('file');
        if (!is_array($photos)) {
            $photos = [$photos];
        }
        

        if (!is_dir($this->photos_path)) {
            mkdir($this->photos_path, 0777);
        }
        error_log('photos count : '.count($photos));
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            error_log('files trying to be uploaded : ' . $photo);
            $name = sha1(date('YmdHis') . str_random(30));
            $save_name = $name . '.' . $photo->getClientOriginalExtension();
            $resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();
            $photo->move($this->photos_path, $save_name);
            
            $upload = new Multimedia();
            $upload->filename = $save_name;
            $upload->original_name = basename($photo->getClientOriginalName());
            $upload->uploader=Auth::user()->id;
            $upload->type="prive";
            if($photo->getClientOriginalExtension()=='jpg' || $photo->getClientOriginalExtension()=='png' || $photo->getClientOriginalExtension()=='jpeg' || $photo->getClientOriginalExtension()=='gif')
                $upload->media_type='Image';
            else
                $upload->media_type='Video';
            $upload->save();
        }
        return redirect('multimedias')->with('success','Fichiers uploadé');
    }

    public function makePublic($id)
    {
        $multimedia=Multimedia::find($id);
        $multimedia->type="public";
        $multimedia->save();
        return redirect('multimedias')->with('success','it became public');
    }
    public function makePrivate($id)
    {
        $multimedia=Multimedia::find($id);
        $multimedia->type="prive";
        $multimedia->save();
        return redirect('multimedias')->with('success','it became private');
    }

    public function delete($id)
    {
        $multimedia=Multimedia::find($id);
        File::delete('multimedia/'.$multimedia->filename);
        $multimedia->delete();
        return redirect('multimedias')->with('success','Multimedia supprimé');
    }

    public function changeTags(Request $request)
    {
        $id=$request->get('id');
        $tags=$request->get('tags');

        $mult=Multimedia::find($id);
        $mult->tags=$tags;
        $mult->save();
    }
}
