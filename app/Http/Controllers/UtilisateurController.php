<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utilisateur;
use App\Right;
use Illuminate\Support\Facades\Hash;
use Storage;
class UtilisateurController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if(Storage::disk('local')->exists('conf.json'))
            $this->middleware('auth');
        else
            return redirect('/conf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utilisateurs=Utilisateur::All();
        return view('utilisateurs.index',compact('utilisateurs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('utilisateurs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'email' => 'email|unique:utilisateurs',
            'nom' => 'min:3',
            'prenom' => 'min:3',
            'telephon' => 'min:8',
            'password' => 'min:6|confirmed',
        ]);
       $utilisateur= new Utilisateur();
       $utilisateur->email=$request->get('email');
       $utilisateur->password= Hash::make($request->get('password'));
       $utilisateur->nom=$request->get('nom');
       $utilisateur->prenom = $request->get('prenom');
       $utilisateur->telephon = $request->get('phon');
       

       $adminData=array('name'=>$request->get('nom'),'email'=>$request->get('email'),
       'password'=>$request->get('password'),'type'=>'3');
       $user = app('App\Http\Controllers\Auth\RegisterController')->create($adminData);
       $utilisateur->userId=$user->id;
       $utilisateur->save();
       return redirect('utilisateurs')->with('success', 'Utilisateur créé');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $utilisateur=Utilisateur::find($id);
        return view('utilisateurs.show',compact('utilisateur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $utilisateur=Utilisateur::find($id);
        return view('utilisateurs.edit',compact('utilisateur','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'email' => 'email',
            'nom' => 'min:3',
            'prenom' => 'min:3',
            'telephon' => 'min:8',
        ]);
       $utilisateur= Utilisateur::find($id);
       $utilisateur->email=$request->get('email');
       $utilisateur->nom=$request->get('nom');
       $utilisateur->prenom = $request->get('prenom');
       $utilisateur->telephon = $request->get('phon');
       $utilisateur->save();
       return redirect('utilisateurs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $utilisateur=Utilisateur::find($id);
        $utilisateur->delete();
        return redirect()->route('utilisateurs.index')->with('success','utilisateur supprimé');
    }

    public function rights($id)
    {
        $utilisateur=Utilisateur::find($id);
        $rights=array();
        $allR=Right::All();
        foreach($allR as $r)
        {
            if($r->utilisateur==$id)
            {
                array_push($rights,$r);
            }
        }
        return view('utilisateurs.right',compact('id','utilisateur','rights'));
    }

    public function updateRights(Request $request,$id)
    {
        $utilisateur=Utilisateur::find($id);
        $rights=Right::All();
        foreach($rights as $right)
        {
            if($right->utilisateur==$id)
            {
                $right->delete();
            }
        }
        $data=explode('-',$request->get('data'));
        foreach($data as $rgh)
        {
            $right=new Right();
            $right->utilisateur=$id;
            $right->name=$rgh;
            $right->save();
        }
        return redirect('utilisateurs')->with('success','droits d\'utilisateur modifié');
    }
}
