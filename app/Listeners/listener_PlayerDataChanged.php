<?php

namespace App\Listeners;

use App\Events\event_PlayerDataChange;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class listener_PlayerDataChanged
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  event_PlayerDataChange  $event
     * @return void
     */
    public function handle(event_PlayerDataChange $event)
    {
        //
    }
}
