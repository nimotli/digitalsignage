Dropzone.options.myDropzone = {
    uploadMultiple: true,
    autoProcessQueue: false,
    url: 'upload_files.php',
    addRemoveLinks: true,
    dictDefaultMessage: 'Upload your files here',
    init: function () {

        var myDropzone = this;

        // Update selector to match your button
        $("#uploadBtn").click(function (e) {
            e.preventDefault();
            myDropzone.processQueue();
        });

        this.on('sending', function(file, xhr, formData) {
            // Append all form inputs to the formData Dropzone will POST
            var data = $('#frmTarget').serializeArray();
            $.each(data, function(key, el) {
                formData.append(el.name, el.value);
            });
        });
    }
}