
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );



        /////////////////////////////////////////
        //Charts                               //
        /////////////////////////////////////////

        //pie chart
        ( function ( $ ) {
        "use strict";
        var ctx = document.getElementById( "pieChart" );
        var dataHolders=ctx.getElementsByTagName('p');
        var asso = parseInt(dataHolders[0].innerHTML);
        var archi = parseInt(dataHolders[1].innerHTML);
        var wait = parseInt(dataHolders[2].innerHTML);
        var total=asso+archi+wait;
        asso=asso*100/total;
        archi=archi*100/total;
        wait=wait*100/total;
        ctx.height = 300;
        var myChart = new Chart( ctx, {
            type: 'pie',
            data: {
                datasets: [ {
                    data: [ asso, archi, wait ],
                    backgroundColor: [
                                        "rgba(0, 123, 255,0.9)",
                                        "rgba(0, 123, 255,0.7)",
                                        "rgba(0, 123, 255,0.5)",
                                        "rgba(0,0,0,0.07)"
                                    ],
                    hoverBackgroundColor: [
                                        "rgba(0, 123, 255,0.9)",
                                        "rgba(0, 123, 255,0.7)",
                                        "rgba(0, 123, 255,0.5)",
                                        "rgba(0,0,0,0.07)"
                                    ]

                                } ],
                labels: [
                                "Associé",
                                "Archivé",
                                "En attent"
                            ]
            },
            options: {
                responsive: true
            }
        } );
    } )( jQuery );