<?php
use App\Events\event_PlayerDataChanged;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard','DashboardController@index')->name('index');
Auth::routes();
Route::get('/', function(){
    return view('home');
});
//Utilisateur routes
////////////////////////////////
Route::resource('utilisateurs','UtilisateurController');
Route::get('utilisateurs/{id}/droits','UtilisateurController@rights');
Route::post('utilisateurs/{id}/droits','UtilisateurController@updateRights');

//Group routes
////////////////////////////////
Route::resource('groups','GroupController');
Route::get('/groups/{id}/disposition','GroupController@disposition');
Route::post('/groups/{id}/disposition','GroupController@updateDispo');
Route::get('/groups/{id}/schedule','GroupController@editSchedule');
Route::post('/groups/{id}/schedule','GroupController@updateSchedule');



//Server configuration routes
////////////////////////////////
Route::get('/conf', 'ServerconfigController@index')->name('configure');
Route::post('conf', 'ServerconfigController@configure');
Route::get('/parameter','ServerconfigController@edit');
Route::post('/parameter','ServerconfigController@update');
//Player routes
////////////////////////////////
Route::resource('player','PlayerController',[
    'except' => ['show']
]);
Route::get('/player/login',function(){
    if(session('loggedinPlayer', 'null')=='null')   
        return view('player.login');
    else
        return view('player.playerhome');
});


Route::get('/player/home','PlayerController@home');
Route::get('/player/logout','PlayerController@logout');
Route::post('/player/login','PlayerController@login');
Route::get('/player/configure','PlayerController@manageConf');
Route::post('/player/configure','PlayerController@configure');
Route::get('/player/{id}/accept','PlayerController@accept');
Route::get('/player/{id}/dissociate','PlayerController@dissociate');
Route::get('/player/{id}/archive','PlayerController@archive');
Route::get('/player/{id}/disposition','PlayerController@disposition');
Route::post('/player/{id}/disposition','PlayerController@updateDispo');
Route::get('/player/{id}/mschedule','PlayerController@schedule');
Route::post('/player/{id}/mschedule','PlayerController@updateSchedule');
Route::get('/player/{id}/schedule','PlayerController@editSchedule');
Route::post('/player/{id}/seschedule','PlayerController@updateSchedule');
Route::get('/part','PlayerController@partindex');
Route::get('/part/{id}/edit','PlayerController@editPart');
Route::post('/part/{id}/edit','PlayerController@updatePart');
Route::get('/player/archived','PlayerController@archivedIndex');
Route::post('/player/home/check','PlayerController@checkIfChanged');
Route::post('/player/home/refresh','PlayerController@SendPlayerDataAjax');
Route::post('/player/processimage','PlayerController@processImage');
Route::post('/player/smart/change','PlayerController@changeMode');

Route::get('/overlay',function(){
    return view('player.overlay');
});
//Upload Media Routes
//////////////////////////////
Route::get('/multimedias','MultimediaController@index')->name('multimedia');
Route::get('/multimedias/upload','MultimediaController@create')->name('multimedia.upload');
Route::post('/multimedias/upload','MultimediaController@store');
Route::get('/multimedias/{id}/pub','MultimediaController@makePublic');
Route::get('/multimedias/{id}/pri','MultimediaController@makePrivate');
Route::get('/multimedias/{id}/del','MultimediaController@delete');
Route::post('/multimedia/tags/change','MultimediaController@changeTags');

//Sequence routes
/////////////////////////////
Route::resource('sequences','SequenceController');

Route::get('/cookietest',function(){
    return view('cookietest');
});
Route::get('/sequences/{id}/supperpose','SequenceController@overlay');
Route::post('/sequences/{id}/supperpose','SequenceController@changeOverlay');

//Schedule routes
////////////////////////////
Route::resource('schedules','ScheduleController');
Route::get('/schedules/{id}/createS','ScheduleController@createSchedule');
Route::post('/schedules/{id}/createS','ScheduleController@makeSchedule');
Route::get('/schedules/{id}/editS','ScheduleController@editSchedule');
Route::post('/schedules/{id}/editS','ScheduleController@updateSchedule');
Route::post('/schedules/getNewCal','ScheduleController@sendNewCalendar');

//custom add Routes
///////////////////////////
Route::resource('annonces','CustomController');

//Broadcasting routes
//////////////////////////
Route::get('/alertbox',function(){
    return view('Broadcasting.evenlistener');
});

Route::get('/fireEvent',function(){
    event(new event_PlayerDataChanged());
});