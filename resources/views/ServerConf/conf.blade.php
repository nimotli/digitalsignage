@extends('layouts.unconfed')

@section('content')
<div class="container bg-light my-5 py-3">
    <h2>Configuration du serveur </h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('ServerconfigController@configure')}}">
            @csrf
            {{csrf_field()}}
            <div class="container border">
                <br>
                <h3>Information d'application</h3>
                <br>
                <div class="form-group">
                    <label for="token">Code Serveur</label>
                    <input type="text" class="form-control" id="token" name="token" placeholder="Code serveur" >
                    <small id="passwordHelpInline" class="text-muted">
                        Utilisé pour connecter les player au serveur.
                      </small>
                </div>
            </div>
            <br>
            <div class="container border">
                <br>
                <h3>Information de client</h3>
                <br>
                <div class="form-group">
                    <label for="rs">Raison social</label>
                    <input type="text" class="form-control" id="rs" name="rs" placeholder="Raison social" >
                </div>
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" >
                </div>
                <div class="form-group">
                    <label for="prenom">Prenom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prenom" >
                </div>
                <div class="form-group">
                    <label for="adresse">Adresse</label>
                    <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Adresse" >
                </div>
                <div class="form-group">
                    <label for="phon">Telephone</label>
                    <input type="text" class="form-control" id="phon" name="phon" placeholder="+212654213645" >
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" >
                </div>
            </div>
            <br>
            <div class="container border">
                <br>
                <h3>Information d'admin</h3>
                <br>
                <div class="form-group">
                    <label for="emaila">Email</label>
                    <input type="text" class="form-control" id="emaila" name="emaila" placeholder="Email" >
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" >
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm mot de passe</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Mot de passe" >
                </div>
                <div class="form-group">
                    <label for="noma">Nom</label>
                    <input type="text" class="form-control" id="noma" name="noma" placeholder="Nom" >
                </div>
                <div class="form-group">
                    <label for="prenoma">Prenom</label>
                    <input type="text" class="form-control" id="prenoma" name="prenoma" placeholder="Prenom" >
                </div>
                <div class="form-group">
                    <label for="phona">Telephon</label>
                    <input type="text" class="form-control" id="phona" name="phona" placeholder="Telephon" >
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Valider</button>
        </form>
</div>
   
@endsection