@extends('layouts.app')

@section('content')

   <div class="container">
       <div class="row">

            <div class="col-xl-3 col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-one">
                                <div class="stat-icon dib"><i class="ti-desktop text-dark border-dark"></i></div>
                                <div class="stat-content dib">
                                    <div class="stat-text">Players</div>
                                    <div class="stat-digit">{{$data['playerCount']}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-one">
                                <div class="stat-icon dib"><i class="ti-gallery text-success border-success"></i></div>
                                <div class="stat-content dib">
                                    <div class="stat-text">Multimedia</div>
                                    <div class="stat-digit">{{$data['multCount']}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-one">
                                <div class="stat-icon dib"><i class="ti-server text-warning border-warning"></i></div>
                                <div class="stat-content dib">
                                    <div class="stat-text">Taille Multimedia</div>
                                    <div class="stat-digit">{{$data['multSize']}}Mo</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-one">
                                <div class="stat-icon dib"><i class="ti-user text-primary border-primary"></i></div>
                                <div class="stat-content dib">
                                    <div class="stat-text">Utilisateurs</div>
                                    <div class="stat-digit">{{$data['users']}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
    

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-3 text-secondary" >Players </h4>
                    </div>
                    <div class="card-body">
                        <canvas id="pieChart">
                            <p style="display:none;">
                                {{$data['playerPie']['associated']}}
                            </p>
                            <p style="display:none;">
                                {{$data['playerPie']['archived']}}
                            </p>
                            <p style="display:none;">
                                {{$data['playerPie']['waiting']}}
                            </p>
                        </canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-3 text-secondary" >L'activité des Players</h4>
                    </div>
                    <div class="card-body" style="overflow-y: scroll;max-height:535px;">
                        <div class="row border-bottom my-1 py-1" >
                            <div class="col-4 border-right">
                                <strong>Player</strong> 
                            </div>
                            <div class="col-4 border-right">
                                <strong>Date</strong> 
                            </div>
                            <div class="col-4">
                                <strong>Position</strong> 
                            </div>
                        </div>
                        @foreach($logs as $log)
                            <div class="row border-bottom my-1 py-1">
                                <div class="col-4">
                                    {{$log->player}}
                                </div>
                                <div class="col-4">
                                    {{$log->login}}
                                </div>
                                <div class="col-4">
                                    {{$log->location}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            
       </div>
       <div class="row">

       </div>
   </div>
    
@endsection