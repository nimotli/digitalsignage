@extends('layouts.unconfed')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center h-75" style="margin-top:20%;background-color:rgba(255,255,255,0.05);">
            <a href="{{url('/dashboard')}}" class="btn btn-success col-md-4 m-2 p-5">
                <h1 class="display-4 text-uppercase text-xl">Serveur</h1>
            </a>
            <a href="{{url('/player/home')}}" class="btn btn-success col-md-4 m-2 p-5">
                <h1 class="display-4 text-uppercase">Player</h1>
            </a>
    </div>
</div>
@endsection
