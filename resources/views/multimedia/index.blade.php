@extends('layouts.app')

@section('content')

<div class="container p-3">
    <div class="row my-2 bg-light py-1 text-right">
        <a href="{{url('multimedias/upload')}}" class="btn btn-primary align-right">Uploader</button><a>
    </div>
    <div class="row p-4 bg-light">
            @foreach($multimedias as $mult)
            @if($mult->uploader==$id || $mult->type=="public")
                <div class="col-md-4">
                    <div class="card text-center">
                        <div class="card-header text-muted">
                                {{$mult->media_type}} | {{$mult->type}}
                        </div>
                        <div class="card-body">
                            @if($mult->media_type=='Video')
                                <video src="{{ asset('multimedia/'.$mult->filename)}}" class="embed-responsive embed-responsive-16by9" controls></video>
                            @else
                                <img src="{{ asset('multimedia/'.$mult->filename)}}" class="multImages">
                            @endif
                        </div>
                        @if( $mult->uploader==$id)
                        <div class="card-footer text-muted">
                            @if($mult->type=="prive")<a href="{{action('MultimediaController@makePublic', $mult['id'])}}" class="btn btn-warning">Rendre public</a>
                            @else<a href="{{action('MultimediaController@makePrivate', $mult['id'])}}" class="btn btn-warning">Rendre privé</a>@endif
                              | <a href="{{action('MultimediaController@delete', $mult['id'])}}" class="btn btn-danger">Supprimer</a>
                              | <button id="tagBtn{{$mult->id}}" class="btn btn-success" onclick="showTagInput('tagsHolder{{$mult->id}}',this)">Tags</button>
                                <div id="tagsHolder{{$mult->id}}" style="display: none;">
                                    <input type="text" class="form-control" id="tagsInput{{$mult->id}}" value="{{$mult->tags}}">
                                    <button class="btn btn-primary" onclick="hideTagInput('{{$mult->id}}',this)" >Enregistrer</button>
                                </div>
                        </div>
                        @endif
                    </div>
                </div>
            @endif
        @endforeach
    </div>
<script>
    function showTagInput(id,elem)
    {
        $(elem).hide(300);
        $('#'+id).show(300);
    }

    function hideTagInput(id,elem)
    {
        $(elem.parentElement).hide(300);
        $('#tagBtn'+id).show(300);
        var tags=document.getElementById('tagsInput'+id).value;
        tags=tags.replace(/[^a-zA-Z0-9]/g,',');
        changeTags(id,tags);
    }

    function changeTags(id,tags)
    {
        $.ajax({
            type:'POST',
            url:'/multimedia/tags/change',
            data:{
            _token: "{{ csrf_token() }}",
            id:id,
            tags: tags,
                },
            success:function(data){
                alert('changed');
            },
            error: function(xhr, textStatus, errorThrown){
                alert(errorThrown);
            }
        });
    }

</script>
@endsection