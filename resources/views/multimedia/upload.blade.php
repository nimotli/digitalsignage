@extends('layouts.app')

@section('content')

<div class="container bg-light py-3" >
    <h3>Upload multimedia</h3>
    <form action="{{url('multimedias/upload')}}" method="post" enctype="multipart/form-data" class="py-3">
        @csrf
        <input class="form-control" type="file" name="file[]" id="file" multiple>
        <button type="submit" class="btn btn-primary my-3">upload</button>
    </form>
</div>
@endsection