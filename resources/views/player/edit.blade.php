@extends('layouts.app')

@section('content')
    <div class="container bg-light my-5 py-3" >
        <h2>Configuration du player </h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('PlayerController@update',$id)}}">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <div class="container border">
                <br>
                <h3>Information d player</h3>
                <br>
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" value="{{$player->nom}}" >
                </div>
                <div class="form-group">
                    <label for="adresse">Adresse</label>
                    <input type="text" class="form-control" id="adresse" name="adresse" value="{{$player->adresse}}" >
                </div>
                <div class="form-group">
                    <label for="cpu">Processeur</label>
                    <input type="text" class="form-control" id="cpu" name="cpu" value="{{$player->cpu}}" >
                </div>
                <div class="form-group">
                    <label for="memoir">Memoire</label>
                    <select class="form-control" name="memoir" id="memoir" autocomplete="off">
                        <option value="512" <?php echo($player->memoir==512)?'selected="selected"':'';?>>512 mo</option>
                        <option value="1000" <?php echo($player->memoir==1000)?'selected="selected"':'';?>>1 go</option>
                        <option value="2000" <?php echo($player->memoir==2000)?'selected="selected"':'';?>>2 go</option>
                        <option value="3000" <?php echo($player->memoir==3000)?'selected="selected"':'';?>>3 go</option>
                        <option value="4000" <?php echo($player->memoir==4000)?'selected="selected"':'';?>>4 go</option>
                        <option value="6000" <?php echo($player->memoir==6000)?'selected="selected"':'';?>>6 go</option>
                        <option value="8000" <?php echo($player->memoir==8000)?'selected="selected"':'';?>>8 go</option>
                        <option value="16000" <?php echo($player->memoir==16000)?'selected="selected"':'';?>>16 go</option>
                        <option value="32000" <?php echo($player->memoir==32000)?'selected="selected"':'';?>>32 go</option>
                    </select>
                </div>
                <br>
                <div class="form-group">
                    <label for="date">Date mise en service</label>
                    <input type="date" class="form-control" id="date" name="date" value="{{$player->datems}}" >
                </div>
                    
            </div>
            <br>
            <div class="container border ">
                <br>
                <h3>Information d'ecran</h3>
                <br>
                <div class="form-group">
                    <label for="marque">Marque</label>
                    <input type="text" class="form-control" id="marque" name="marque" value="{{$player->marque}}" >
                </div>
                <div class="form-group">
                    <label for="type">Type</label>
                    <select name="type" id="type" name="type" class="form-control" autocomplete="off">
                        <option value="LCD" <?php echo($player->type=='LCD')?'selected="selected"':'';?>>LCD</option>
                        <option value="LED" <?php echo($player->type=='LED')?'selected="selected"':'';?>>LED</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="taille">Taille</label>
                    <select name="taille" id="taille" class="form-control" autocomplete="off">
                        <option () value="7" <?php echo($player->taille==7)?'selected="selected"':'';?>>7"</option>
                        <option value="11" <?php echo($player->taille==11)?'selected="selected"':'';?>>11"</option>
                        <option value="13" <?php echo($player->taille==13)?'selected="selected"':'';?>>13"</option>
                        <option value="15" <?php echo($player->taille==15)?'selected="selected"':'';?>>15"</option>
                        <option value="17" <?php echo($player->taille==17)?'selected="selected"':'';?>>17"</option>
                    </select>                
                </div>
                <div class="form-group">
                    <label for="ar">Ration d'aspect</label>
                    <select name="ar" id="ar" class="form-control" autocomplete="off">
                        <option value="5:4" <?php echo($player->ar=='5:4')?'selected="selected"':'';?>>5:4</option>
                        <option value="4:3" <?php echo($player->ar=='4:3')?'selected="selected"':'';?>>4:3</option>
                        <option value="16:9" <?php echo($player->ar=='16:9')?'selected="selected"':'';?>>16:9</option>
                        <option value="16:10" <?php echo($player->ar=='16:10')?'selected="selected"':'';?>>16:10</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="resolution">Resolution</label>
                    <select name="resolution" id="resolution" class="form-control" autocomplete="off">
                        <option value="1024×576" <?php echo($player->resolution=='1024×576')?'selected="selected"':'';?>>1024×576</option>
                        <option value="1152×648" <?php echo($player->resolution=='1152×648')?'selected="selected"':'';?>>1152×648</option>
                        <option value="1280×720" <?php echo($player->resolution=='1280×720')?'selected="selected"':'';?>>1280×720</option>
                        <option value="1366×768" <?php echo($player->resolution=='1366×768')?'selected="selected"':'';?>>1366×768</option>
                        <option value="1600×900" <?php echo($player->resolution=='1600×900')?'selected="selected"':'';?>>1600×900</option>
                        <option value="1920×1080" <?php echo($player->resolution=='1920×1080')?'selected="selected"':'';?>>1920×1080</option>
                    </select>                
                </div>
                <div class="form-group">
                    <label for="dated">Date mise en service</label>
                    <input type="date" class="form-control" id="dated" name="dated" value="{{$player->sdatems}}" >
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Modifier</button>
        </form>
    </div>
@endsection