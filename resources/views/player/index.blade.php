@extends('layouts.app')

@section('content')

<style>

    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
      }
      
      /* Hide default HTML checkbox */
      .switch input {display:none;}
      
      /* The slider */
      .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
      }
      
      .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
      }
      
      input:checked + .slider {
        background-color: #2196F3;
      }
      
      input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
      }
      
      input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
      }
      
      /* Rounded sliders */
      .slider.round {
        border-radius: 34px;
      }
      
      .slider.round:before {
        border-radius: 50%;
      }

</style>
    <div class="content mt-3">
        @if (\Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Players</strong>
                            <a href="{{ url('/player/archived') }}" class="float-right btn btn-primary">Players Archivé</a>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <!--<th>Id</th>-->
                                        <th>Nom</th>
                                        <th>Adresse</th>
                                        <!--<th>Cpu</th>-->
                                        <!--<th>Memoire</th>-->
                                        <!--<th>Marque ecran</th>-->
                                        <!--<th>Type ecran</th>-->
                                        <th>Taille</th>
                                        <th>Resolution</th>
                                        <th>Ratio d aspect</th>
                                        <th>Date mise en service</th>
                                        <th>Smart mode</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($players as $player)
                                        @if ($player['archived'] != 1)
                                            <tr>
                                                <!--<td>{{$player['id']}}</td>-->
                                                <td>{{$player['nom']}}</td>
                                                <td>{{$player['adresse']}}</td>
                                               <!-- <td>{{$player['cpu']}}</td>-->
                                               <!-- <td>{{$player['memoir']}}</td>-->
                                                <!--<td>{{$player['marque']}}</td>-->
                                                <!--<td>{{$player['type']}}</td>-->
                                                <td>{{$player['taille']}}</td>
                                                <td>{{$player['resolution']}}</td>
                                                <td>{{$player['ar']}}</td>
                                                <td>{{$player['datems']}}</td>
                                                <td><label class="switch">
                                                    <input type="checkbox" onchange="modeChange(this.id,this)" id="{{$player['id']}}" @if($player['smart']==1) checked @endif>
                                                    <span class="slider round"></span>
                                                  </label></td>
                                                @if($player->accepted==0)
                                                <td>
                                                    <a href="{{action('PlayerController@accept', $player['id'])}}" class="btn btn-dark">Accepter</a>
                                                
                                                    <a href="{{action('PlayerController@archive', $player['id'])}}" class="btn btn-secondary">Archiver</a>
                                                </td>
                                                @else
                                                <td>
                                                    @if($player->schedule==-1)
                                                        <a href="{{action('PlayerController@schedule', $player['id'])}}" class="btn btn-dark">Planner</a>
                                                    @else
                                                        <a href="{{action('PlayerController@editSchedule', $player['id'])}}" class="btn btn-dark">Modifier planning</a>
                                                    @endif
                                                    <a href="{{action('PlayerController@disposition', $player['id'])}}" class="btn btn-success">Disposition</a>
                                                    <a href="{{action('PlayerController@edit', $player['id'])}}" class="btn btn-warning">Modifier</a>
                                                    <a href="{{action('PlayerController@dissociate', $player['id'])}}" class="btn btn-danger">Dissocier</a>
                                                </td>
                                                @endif
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
    <script>
        function modeChange(id,element)
        {
            var checked=$(element).is(":checked");
            
            
            $.ajax({
                type:'POST',
                url:'/player/smart/change',
                data:{
                    _token: "{{ csrf_token() }}",
                    player: id,
                    checked :checked,
                },
                success:function(data){
                },
                error: function(xhr, textStatus, errorThrown){
                }
            });
        }
    </script>
@endsection