@extends('layouts.unconfed')

@section('content')
<div id="adHolder" unselectable="on" onselectstart="return false;" style="height: 721.438px; width: 1281px;">  
        <div id="adHolder" unselectable="on" onselectstart="return false;" style="height: 720.313px; width: 1279px;"><div onclick="select(this)" class="draggable" id="" style="border: 1px solid grey; display: block; background-color: rgba(49, 27, 209, 0.7); transform: translate(-1px, 669.317px); z-index: 0; width: 1279px; height: 50px; position: absolute;" data-x="-1" data-y="669.316650390625">
    <a href="#" onclick="removeRegion(this,event)" onmouseup="fit(this)" style="z-index:10;"><i class="fa fa-times float-right"></i></a>
    <p style="display:none;">text</p>
<div style="font-size: 30px; text-align: center; color: rgb(255, 255, 255);">All rights reserved</div></div>  
        <div id="adHolder" unselectable="on" onselectstart="return false;" style="height: 719.188px; width: 1277px;">  
        <div id="adHolder" unselectable="on" onselectstart="return false;" style="height: 718.063px; width: 1275px;">  
        <div id="adHolder" unselectable="on" onselectstart="return false;" style="height: 716.938px; width: 1273px;"><div onclick="select(this)" class="draggable" id="" style="border: 1px solid grey; display: block; background-color: transparent; z-index: 0; background-image: url(&quot;http://localhost:8000/multimedia/77fddba216998154caa42250b2843c54d914ad4c.png&quot;); background-size: cover; position: absolute; height: 150px; width: 150px; transform: translate(-1px, -1px);" data-x="-1" data-y="-1">
    <a href="#" onclick="removeRegion(this,event)" onmouseup="fit(this)" style="z-index:10;"><i class="fa fa-times float-right"></i></a>
    <p style="display:none;">image</p>
</div><div onclick="select(this)" class="draggable" id="" style="border: 1px dashed white; display: block; background-color: transparent; transform: translate(324px, -1px); z-index: 10; width: 948px; height: 150px; position: absolute;" data-x="324" data-y="-1">
    <a href="#" onclick="removeRegion(this,event)" onmouseup="fit(this)" style="z-index:10;"><i class="fa fa-times float-right"></i></a>
    <p style="display:none;">text</p>
<div style="font-size: 92px; color: rgb(255, 255, 255);">Company name</div></div><div onclick="select(this)" class="draggable" id="" style="border: 1px solid grey; display: block; background-color: rgba(0, 0, 0, 0.7); z-index: 0; transform: translate(156px, 193px); width: 950px; height: 466px; position: absolute;" data-x="156" data-y="193">
    <a href="#" onclick="removeRegion(this,event)" onmouseup="fit(this)" style="z-index:10;"><i class="fa fa-times float-right"></i></a>
    <p style="display:none;">text</p>
<div style="font-size: 16px; color: rgb(255, 255, 255); text-align: center;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
Why do we use it?

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
Why do we use it?

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
</div></div>  
        
    </div>
    </div>
    </div>
    </div>
    </div>

<script>
    var maxHeight=$(window).height();
    var maxWidth=$(window).width();
    scaleOverlay()
        

    ///////////////////////////////////
    //OVERLAY MANAGEMENET//////////////
    ///////////////////////////////////
    function scaleOverlay()
    {
        var element= document.getElementById('adHolder');
        var overlayHeight=$(element).height();
        var overlayWidth=$(element).width();
        //alert('max height '+maxHeight+' max width '+maxWidth+' height '+overlayHeight+' width '+overlayWidth);

        var scaleX=maxWidth/overlayWidth;
        var scaleY=maxHeight/overlayHeight;
        var scale=scaleX+','+scaleY;
        //alert('('+scale+')');
        
        $(element).css({
        'transform-origin'  : 'top left',
        '-webkit-transform' : 'scale(' + scale + ')',
        '-moz-transform'    : 'scale(' + scale + ')',
        '-ms-transform'     : 'scale(' + scale + ')',
        '-o-transform'      : 'scale(' + scale + ')',
        'transform'         : 'scale(' + scale + ')'
        });
        clean();
    }

    function clean()
    {
        var element= document.getElementById('adHolder');
        var link=element.getElementsByTagName('a');
        for(i=0;i<link.length;i++)
        {
            $(link[i]).hide();
        }
        var divs=element.getElementsByTagName('div');
        for(i=0;i<divs.length;i++)
        {
            $(divs[i]).css('border','');
        }
    }
</script>
@endsection

