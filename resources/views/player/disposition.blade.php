@extends('layouts.app')

@section('content')
    <?php $ar=\App\Http\Controllers\Auth\LoginController::getAccessRights(); ?>
    <div class="container bg-light my-5 py-3" >
        <h2>Configuration du player </h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('PlayerController@updateDispo',$id)}}">
            @csrf
            <div class="container border">
                <br>
                <h3>Dispositions</h3>
                <br>
                <div class=" mx-auto">
                    <select name="" id="layout">
                        <option @if($dis!=null && $dis->type==0)selected @endif data-img-class="first" data-img-alt="Séquence seule" value="0" data-img-src="{{ asset('images/Sequence-Seul.png') }}">Default</option>
                        <option @if($dis!=null && $dis->type==1)selected @endif data-img-alt="Séquence avec une bande avec texte défilant" value="1" data-img-src="{{ asset('images/Sequence-Seul-Bar.png') }}">Single bar</option>
                        <option @if($dis!=null && $dis->type==2)selected @endif data-img-alt="Séquence avec deux bandes" value="2" data-img-src="{{ asset('images/Sequence-Seul-Deux-Bar.png') }}">Two bars</option>
                        @if($ar=='admin')
                        <option @if($dis!=null && $dis->type==3)selected @endif data-img-alt="Quatre séquences publicitaires simultanément" value="3" data-img-src="{{ asset('images/Sequence-Quatres.png') }}">Four sequences</option>
                        @endif
                    </select>
                </div>
                <div class="form-group" id="pos">
                    <label for="pos">Position bande</label>
                    <select name="pos" id="pos" class="form-control">
                        <option @if($dis!=null && $dis->pos==0)selected @endif value="0">top</option>
                        <option @if($dis!=null && $dis->pos==1)selected @endif value="1">bot</option>
                    </select>
                </div>
                <div class="form-group" id="txt1">
                    <label for="topbar">Text bande</label>
                    <input type="text" name="txt1" id="topbar" class="form-control" @if($dis!=null) value="{{$dis->text1}}" @endif >
                </div>
                <div class="form-group" id="txt2">
                    <label for="botbar">Text deuxieme bande</label>
                    <input type="text" name="txt2" id="botbar" class="form-control" @if($dis!=null) value="{{$dis->text2}}" @endif >
                </div>
                <input type="hidden" name="type" id="type" @if($dis!=null) value="{{$dis->type}}" @endif >
                <br>
                <div id="4seq">
                    <div class="row border border-secondary">
                        <div class="col border-right border-secondary p-3">
                            <label for="txttl">Utilisateur</label>
                            <select name="txttl" id="txttl" class="form-control">
                                @foreach($utilisateurs as $util)
                                    <option value="{{$util->id}}">{{$util->nom}} {{$util->prenom}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col p-3">
                            <label for="txttr">Utilisateur</label>
                            <select name="txttr" id="txttr" class="form-control">
                                @foreach($utilisateurs as $util)
                                    <option value="{{$util->id}}">{{$util->nom}} {{$util->prenom}}</option>
                                @endforeach
                            </select>                    
                        </div>
                    </div>
                    <div class="row border-bottom border-right border-left border-secondary">
                        <div class="col border-right border-secondary p-3">
                            <label for="txtbl">Utilisateur</label>
                            <select name="txtbl" id="txtbl" class="form-control">
                                @foreach($utilisateurs as $util)
                                    <option value="{{$util->id}}">{{$util->nom}} {{$util->prenom}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col p-3">
                            <label for="txttr">Utilisateur</label>
                            <select name="txtbr" id="txtbr" class="form-control">
                                @foreach($utilisateurs as $util)
                                    <option value="{{$util->id}}">{{$util->nom}} {{$util->prenom}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                
            </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Modifier</button>
        </form>
    </div>
    <script>

        $("#layout").imagepicker({
            show_label: true
            });
            $('#pos').hide();
            $('#txt1').hide();
            $('#txt2').hide();

            $('#layout').on('change', function() {
                if( this.value == 1)
                {
                    $('#pos').show(300);
                    $('#txt1').show(300);
                    $('#txt2').hide(300);
                    $('#4seq').hide(300);
                    $('#type').val(1);
                }
                else if(this.value == 2)
                {
                    $('#pos').hide(300);
                    $('#txt1').show(300);
                    $('#txt2').show(300);
                    $('#4seq').hide(300);
                    $('#type').val(2);
                }
                else if(this.value == 0)
                {
                    $('#pos').hide(300);
                    $('#txt1').hide(300);
                    $('#txt2').hide(300);
                    $('#4seq').hide(300);
                    $('#type').val(0);
                }
                else
                {
                    $('#pos').hide(300);
                    $('#txt1').hide(300);
                    $('#txt2').hide(300);
                    $('#4seq').show(300);
                    $('#type').val(4);
                }
            });
        $( document ).ready(function() {
            var slctedval = $('#layout').val();
            if(slctedval==1)
            {
                $('#pos').show(300);
                $('#txt1').show(300);
                $('#txt2').hide(300);
                $('#4seq').hide(300);
                $('#type').val(1);
            }
            else if(slctedval==2)
            {
                $('#pos').hide(300);
                $('#txt1').show(300);
                $('#txt2').show(300);
                $('#4seq').hide(300);
                $('#type').val(2);
            }
            else if(slctedval==0)
            {
                $('#pos').hide(300);
                $('#txt1').hide(300);
                $('#txt2').hide(300);
                $('#4seq').hide(300);
                $('#type').val(0);
            }
            else 
            {
                $('#pos').hide(300);
                $('#txt1').hide(300);
                $('#txt2').hide(300);
                $('#4seq').show(300);
                $('#type').val(4);
            }
        });
    </script>
@endsection