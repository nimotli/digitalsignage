@extends('layouts.app')

@section('content')
    <div class="container bg-light my-5 py-3" >
        <h2>Configuration du player </h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{url('player')}}">
            @csrf
            {{csrf_field()}}
            
            <div class="container border">
                <br>
                <h3>Information d player</h3>
                <br>
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" >
                </div>
                <div class="form-group">
                    <label for="adresse">Adresse</label>
                    <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Adresse" >
                </div>
                <div class="form-group">
                    <label for="cpu">Processeur</label>
                    <input type="text" class="form-control" id="cpu" name="cpu" placeholder="intel i7 7700k" >
                </div>
                <div class="form-group">
                    <label for="memoir">Memoire</label>
                    <select class="form-control" name="memoir" id="memoir">
                        <option value="512">512 mo</option>
                        <option value="1000">1 go</option>
                        <option value="2000">2 go</option>
                        <option value="3000">3 go</option>
                        <option value="4000">4 go</option>
                        <option value="6000">6 go</option>
                        <option value="8000">8 go</option>
                        <option value="16000">16 go</option>
                        <option value="32000">32 go</option>
                    </select>
                </div>
                <br>
                <div class="form-group">
                    <label for="date">Date mise en service</label>
                    <input type="date" class="form-control" id="date" name="date" placeholder="Date mise en service" >
                </div>
                    
            </div>
            <br>
            <div class="container border ">
                <br>
                <h3>Information d'ecran</h3>
                <br>
                <div class="form-group">
                    <label for="marque">Marque</label>
                    <input type="text" class="form-control" id="marque" name="marque" placeholder="Marque" >
                </div>
                <div class="form-group">
                    <label for="type">Type</label>
                    <select name="type" id="type" name="type" class="form-control">
                        <option value="LCD">LCD</option>
                        <option value="LED">LED</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="taille">Taille</label>
                    <select name="taille" id="taille" class="form-control">
                        <option value="7">7"</option>
                        <option value="11">11"</option>
                        <option value="13">13"</option>
                        <option value="15">15"</option>
                        <option value="17">17"</option>
                    </select>                
                </div>
                <div class="form-group">
                    <label for="ar">Ration d'aspect</label>
                    <select name="ar" id="ar" class="form-control">
                        <option value="5:4">5:4</option>
                        <option value="4:3">4:3</option>
                        <option value="16:9">16:9</option>
                        <option value="16:10">16:10</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="resolution">Resolution</label>
                    <select name="resolution" id="resolution" class="form-control">
                        <option value="5:4">1024×576</option>
                        <option value="4:3">1152×648</option>
                        <option value="16:9">1280×720</option>
                        <option value="16:10">1366×768</option>
                        <option value="16:10">1600×900</option>
                        <option value="16:10">1920×1080</option>
                    </select>                
                </div>
                <div class="form-group">
                    <label for="dated">Date mise en service</label>
                    <input type="date" class="form-control" id="dated" name="dated" placeholder="Date" >
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Valider</button>
        </form>
    </div>
@endsection