@extends('layouts.unconfed')

@section('content')
<style>
    #adHolder{display: block !important;}
    .ai{
        position:absolute;z-index: 10000;bottom:0;right:0;width:175 ;height:175;z-index: 10000;;
      }
</style>
<div id="camHolder" style="z-index:-1">
    <video class="ai" id="video" width="175" height"175" preload autoplay loop muted></video>
    <canvas class="ai" width="175" height"175" id="canvas" ></canvas>
    <canvas style="z-index:0;" class="ai" width="175" height"175" id="hidden-canvas" ></canvas>
    <img style="display: none;" class="ai" id="snap">
</div>
<p style="display:none;" id="playerIdHolder">{{$playerId}}</p>
<div id="loading" style="display:none;position:fixed;height: 100vh;width: 100%;left:0;top:0;background-color:rgb(50,50,50);z-index: 10000;">
    <center><p style="font-size: 5em;left:35%;top:40%;position :fixed;color:#ccc;">LOADING...</p><center>
</div>

<div id="mainDiv">


    

@if($playerDisp!=null)
    @if($playerDisp->type==2)
    <div style="height:100vh;">
        <div style="position:relative;background-color:black;height:7vh;width:100%;z-index:5000;">
            <marquee behavior="scroll" direction="left"><h4 style="color:white;">{{$playerDisp->text1}}</h4></marquee>
        </div>
        <div  style="position:relative;height:86vh;width:100%;overflow:hidden;">
            @foreach($allowedSequences as $sequence)
                @if($sequence[1]!=null)
                    <div class="overlays" id="overlay{{$sequence[1]->id}}" style="display:none">{!! $sequence[1]->html !!}</div>
                @endif
            @endforeach
            <ul id="slides" style="position:absolute; height:100%;width:100%;background-color:black;">
                @foreach($allowedSequences as $sequence)
                    @foreach($allMs as $ms)
                        @if($ms->sequence==$sequence[0]->id)
                            @if(explode('.',$ms->filename)[1]=="mp4" || explode('.',$ms->filename)[1]=="3gp"|| explode('.',$ms->filename)[1]=="mov"|| explode('.',$ms->filename)[1]=="wmv"|| explode('.',$ms->filename)[1]=="flv"|| explode('.',$ms->filename)[1]=="avi")
                                <li style="display:none;position:fixed;top:7vh;width:100%;">
                                    <video src="{{ asset('multimedia/'.$ms->filename)}}" style="min-height:86vh;min-width:100%;" autoplay loop></video>
                                    <p style="diplay:none;">{{$ms->duree}}</p>
                                    <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                                </li>
                            @else
                                <li style="display:none;position:fixed;top:7vh;width:100%;">
                                    <img src="{{ asset('multimedia/'.$ms->filename)}}" style="height:86vh;width:100%;">
                                    <p style="diplay:none;">{{$ms->duree}}</p>
                                    <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                                </li>
                            @endif
                        @endif
                    @endforeach
                @endforeach
            </ul>
        </div>
        <div style="position:relative;background-color:black;height:7vh;width:100%;z-index:5000;">
            <marquee behavior="scroll" direction="left"><h4 style="color:white;">{{$playerDisp->text1}}</h4></marquee>
        </div>
    </div>
        
    @elseif($playerDisp->type==1 && $playerDisp->pos==0)
        <div style="position:relative;background-color:black;height:7vh;width:100%;z-index:5000;">
            <marquee behavior="scroll" direction="left"><h4 style="color:white;">{{$playerDisp->text1}}</h4></marquee>
        </div>
        <div  style="position:relative;height:93vh;width:100%;overflow:hidden;">
            @foreach($allowedSequences as $sequence)
                @if($sequence[1]!=null)
                    <div class="overlays" id="overlay{{$sequence[1]->id}}" style="display:none">{!! $sequence[1]->html !!}</div>
                @endif
            @endforeach
            <ul id="slides" style="position:absolute; height:100%;width:100%;background-color:black;">
                @foreach($allowedSequences as $sequence)
                    @foreach($allMs as $ms)
                        @if($ms->sequence==$sequence[0]->id)
                            @if(explode('.',$ms->filename)[1]=="mp4" || explode('.',$ms->filename)[1]=="3gp"|| explode('.',$ms->filename)[1]=="mov"|| explode('.',$ms->filename)[1]=="wmv"|| explode('.',$ms->filename)[1]=="flv"|| explode('.',$ms->filename)[1]=="avi")
                                <li style="display:none;position:fixed;top:7vh;width:100%;">
                                    <video src="{{ asset('multimedia/'.$ms->filename)}}" style="min-height:93vh;min-width:100%;" autoplay loop></video>
                                    <p style="diplay:none;">{{$ms->duree}}</p>
                                    <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                                </li>
                            @else
                                <li style="display:none;position:fixed;top:7vh;width:100%;">
                                    <img src="{{ asset('multimedia/'.$ms->filename)}}" style="height:93vh;width:100%;">
                                    <p style="diplay:none;">{{$ms->duree}}</p>
                                    <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                                </li>
                            @endif
                        @endif
                    @endforeach
                @endforeach
            </ul>
        </div>
    @elseif($playerDisp->type==1 && $playerDisp->pos==1)
        <div  style="position:relative;height:93vh;width:100%;overflow:hidden;">
            @foreach($allowedSequences as $sequence)
                @if($sequence[1]!=null)
                    <div class="overlays" id="overlay{{$sequence[1]->id}}" style="display:none">{!! $sequence[1]->html !!}</div>
                @endif
            @endforeach
            <ul id="slides" style="position:absolute; height:100%;width:100%;background-color:black;">
                @foreach($allowedSequences as $sequence)
                    @foreach($allMs as $ms)
                        @if($ms->sequence==$sequence[0]->id)
                            @if(explode('.',$ms->filename)[1]=="mp4" || explode('.',$ms->filename)[1]=="3gp"|| explode('.',$ms->filename)[1]=="mov"|| explode('.',$ms->filename)[1]=="wmv"|| explode('.',$ms->filename)[1]=="flv"|| explode('.',$ms->filename)[1]=="avi")
                                <li style="display:none;position:fixed;bot:7vh;width:100%;">
                                    <video src="{{ asset('multimedia/'.$ms->filename)}}" style="min-height:93vh;min-width:100%;" autoplay loop></video>
                                    <p style="diplay:none;">{{$ms->duree}}</p>
                                    <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                                </li>
                            @else
                                <li style="display:none;position:fixed;bot:7vh;width:100%;">
                                    <img src="{{ asset('multimedia/'.$ms->filename)}}" style="height:93vh;width:100%;">
                                    <p style="diplay:none;">{{$ms->duree}}</p>
                                    <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                                </li>
                            @endif
                        @endif
                    @endforeach
                @endforeach
            </ul>
        </div>
        <div style="position:relative;background-color:black;height:7vh;width:100%;z-index:5000;">
            <marquee behavior="scroll" direction="left"><h4 style="color:white;">{{$playerDisp->text1}}</h4></marquee>
        </div>
    @elseif($playerDisp->type==0)
        <div  style="position:relative;height:100vh;width:100%;overflow: hidden;">
            @foreach($allowedSequences as $sequence)
                @if($sequence[1]!=null)
                    <div class="overlays" id="overlay{{$sequence[1]->id}}" style="display:none">{!! $sequence[1]->html !!}</div>
                @endif
            @endforeach
            <ul id="slides" style="position:absolute; height:100%;width:100%;background-color:black;">
                @foreach($allowedSequences as $sequence)
                    @foreach($allMs as $ms)
                        @if($ms->sequence==$sequence[0]->id)
                            @if(explode('.',$ms->filename)[1]=="mp4" || explode('.',$ms->filename)[1]=="3gp"|| explode('.',$ms->filename)[1]=="mov"|| explode('.',$ms->filename)[1]=="wmv"|| explode('.',$ms->filename)[1]=="flv"|| explode('.',$ms->filename)[1]=="avi")
                                <li style="display:none;position:fixed;top:0;width:100%;">
                                    <video src="{{ asset('multimedia/'.$ms->filename)}}" style="min-height:100vh;min-width:100%;" autoplay loop></video>
                                    <p style="diplay:none;">{{$ms->duree}}</p>
                                    <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                                </li>
                            @else
                                <li style="display:none;position:fixed;top:0;width:100%;">
                                    <img src="{{ asset('multimedia/'.$ms->filename)}}" style="height:100vh;width:100%;">
                                    <p style="diplay:none;">{{$ms->duree}}</p>
                                    <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                                </li>
                            @endif
                        @endif
                    @endforeach
                @endforeach
                
            </ul>
        </div>
    @elseif($playerDisp->type==4)
        <?php $mults=\App\Http\Controllers\PlayerController::getplayerMult($playerId); ?>
        <div class="border" style="height: 100vh;width:100%;position:absolute;top:0;left:0;overflow:hidden;background-color:black;">
            <div class="row p-0 m-0" style="height:50vh;">
                <div class="col-6 p-0 border-right border-bottom">
                    @if(isset($mults[0]))
                    @if(explode('.',$mults[0]->filename)[1]=="mp4" || explode('.',$mults[0]->filename)[1]=="3gp"|| explode('.',$mults[0]->filename)[1]=="mov"|| explode('.',$mults[0]->filename)[1]=="wmv"|| explode('.',$mults[0]->filename)[1]=="flv"|| explode('.',$mults[0]->filename)[1]=="avi")
                        <video src="{{ asset('multimedia/'.$mults[0]->filename)}}" style="height:50vh;width:100%;" autoplay loop></video>
                    @else
                        <img src="{{ asset('multimedia/'.$mults[0]->filename)}}" style="height:50vh;width:100%;">
                    @endif
                    @endif
                </div> 
                <div class="col-6 p-0">
                    @if(isset($mults[1]))
                    @if(explode('.',$mults[1]->filename)[1]=="mp4" || explode('.',$mults[1]->filename)[1]=="3gp"|| explode('.',$mults[1]->filename)[1]=="mov"|| explode('.',$mults[1]->filename)[1]=="wmv"|| explode('.',$mults[1]->filename)[1]=="flv"|| explode('.',$mults[1]->filename)[1]=="avi")
                        <video src="{{ asset('multimedia/'.$mults[1]->filename)}}" style="height:50vh;width:100%;" autoplay loop></video>
                    @else
                        <img src="{{ asset('multimedia/'.$mults[1]->filename)}}" style="height:50vh;width:100%;">
                    @endif
                    @endif
                </div>
            </div>
            <div class="row p-0" style="height:50vh;">
                <div class="col-6 p-0 m-0 border-right">
                    @if(isset($mults[2]))
                    @if(explode('.',$mults[2]->filename)[1]=="mp4" || explode('.',$mults[2]->filename)[1]=="3gp"|| explode('.',$mults[2]->filename)[1]=="mov"|| explode('.',$mults[2]->filename)[1]=="wmv"|| explode('.',$mults[2]->filename)[1]=="flv"|| explode('.',$mults[2]->filename)[1]=="avi")
                        <video src="{{ asset('multimedia/'.$mults[2]->filename)}}" style="height:50vh;width:100%;" autoplay loop></video>
                    @else
                        <img src="{{ asset('multimedia/'.$mults[2]->filename)}}" style="height:50vh%;width:100%;">
                    @endif
                    @endif
                </div> 
                <div class="col-6 p-0 border-top">
                    @if(isset($mults[3]))
                    @if(explode('.',$mults[3]->filename)[1]=="mp4" || explode('.',$mults[3]->filename)[1]=="3gp"|| explode('.',$mults[3]->filename)[1]=="mov"|| explode('.',$mults[3]->filename)[1]=="wmv"|| explode('.',$mults[3]->filename)[1]=="flv"|| explode('.',$mults[3]->filename)[1]=="avi")
                        <video src="{{ asset('multimedia/'.$mults[3]->filename)}}" style="height:50vh;width:100%;" autoplay loop></video>
                    @else
                        <img src="{{ asset('multimedia/'.$mults[3]->filename)}}" style="height:50vh;width:100%;">
                    @endif
                    @endif
                </div>
            </div>
        </div>
    @endif
@else
    <div  style="position:relative;height:100vh;width:100%;overflow: hidden;">
        @foreach($allowedSequences as $sequence)
            @if($sequence[1]!=null)
                <div class="overlays" id="overlay{{$sequence[1]->id}}" style="display:none">{!! $sequence[1]->html !!}</div>
            @endif
        @endforeach
        <ul id="slides" style="position:absolute; height:100%;width:100%;background-color:black;">
            @foreach($allowedSequences as $sequence)
                @foreach($allMs as $ms)
                    @if($ms->sequence==$sequence->id)
                        @if(explode('.',$ms->filename)[1]=="mp4" || explode('.',$ms->filename)[1]=="3gp"|| explode('.',$ms->filename)[1]=="mov"|| explode('.',$ms->filename)[1]=="wmv"|| explode('.',$ms->filename)[1]=="flv"|| explode('.',$ms->filename)[1]=="avi")
                            <li style="display:none;position:fixed;width:100%;">
                                <video src="{{ asset('multimedia/'.$ms->filename)}}" style="min-height:100vh;min-width:100%;" autoplay loop></video>
                                <p style="diplay:none;">{{$ms->duree}}</p>
                                <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                            </li>
                        @else
                            <li style="display:none;position:fixed;width:100%;">
                                <img src="{{ asset('multimedia/'.$ms->filename)}}" style="height:100vg;width:100%;">
                                <p style="diplay:none;">{{$ms->duree}}</p>
                                <p style="diplay:none;">{{$sequence[0]->overlay}}</p>
                            </li>
                        @endif
                    @endif
                @endforeach
            @endforeach
        </ul>
    </div>
@endif

</div>
<script>
    var timeoutCall;
    var slides=document.getElementById("slides").children;
    var slidesNbr=slides.length;
    var showingElement=0;
    var showed=false;
    var maxHeight=$(window).height();
    var maxWidth=$(window).width();
    scaleOverlay();
    if(slidesNbr>0)
        nextSlider();
    function verifyValues()
    {
        if(showingElement<0)
            showingElement=0;
        if(showingElement>=slidesNbr)
            showingElement=0;
    }
    function callAfter(time)
    {
        timeoutCall = setTimeout(function() { nextSlider(); }, time);
    }
    function nextSlider()
    {
        var overlayid = 'overlay'+slides[showingElement].getElementsByTagName("p")[1].innerHTML;
        if(slidesNbr>1)
        {
            var theIndex=-1;
            if(showingElement>0)
                theIndex=showingElement-1;
            else
                theIndex=slidesNbr-1;

            $(slides[showingElement]).css('z-index', 0);
            $(slides[showingElement]).show();
            $(slides[showingElement]).fadeTo(50,1);

            $(slides[theIndex]).css('z-index', 10);
            $(slides[theIndex]).fadeTo(500,0.01,function(){
                $(slides[theIndex]).hide();
            });
            
            callAfter(parseFloat(slides[showingElement].getElementsByTagName("p")[0].innerHTML)*1000);
            if(showingElement==slidesNbr-1)
                showingElement=0;
            else
                showingElement++;
        }
        else
        {
            $(slides[showingElement]).show();
            callAfter(parseFloat(slides[showingElement].getElementsByTagName("p")[0].innerHTML)*1000);
        }
        if(!$('#'+overlayid).is(":visible"))
        {
            var overlays=document.getElementsByClassName('overlays');
            for(j=0;j<overlays.length;j++)
            {
                if(overlayid!=overlays[j].id)
                {
                    if($(overlays[j]).is(":visible"))
                        $(overlays[j]).hide();
                }
            }
            $('#'+overlayid).show(100);
        }

        
    }

    
    function insertRecievedAd(ad)
    {
        ad=ad.split('-');
        var slidesHolder=document.getElementById("slides");
        var publicPath=ad[4];
        var filename=ad[1],filetype=ad[2];
        var itemToAdd="img";
        var elemHeight=$(slidesHolder.parentElement.innerHTML).height();
        var elem=null,li=null;
        if(filetype=='Video')
        {
            li=document.createElement('li');
            elem = document.createElement('video');
            $(elem).css('min-height',elemHeight);
            $(elem).css('min-width','100%');
            $(elem).attr('src','multimedia\\'+filename);
            li.appendChild(elem);
        }
        else
        {
            li=document.createElement('li');
            elem = document.createElement('img');
            $(elem).css('height',elemHeight);
            $(elem).css('width','100%');
            $(elem).attr('src','/multimedia/'+filename);
            li.appendChild(elem);
        }
        if(slidesNbr>0)
            slidesHolder.insertBefore(li, slides[showingElement+1]);
        else
            slidesHolder.appendChild(li);

    }

    ///////////////////////////////////
    //OVERLAY MANAGEMENET//////////////
    ///////////////////////////////////
    function scaleOverlay()
    {
        var element= document.getElementsByClassName('overlays');
        var i=0;
        while(i<element.length)
        {
            var elem=element[i].getElementsByTagName('div')[0];
            $(elem).css('z-index',5000);
            $(elem).css('position','fixed');
            var overlayHeight=$(elem).height();
            var overlayWidth=$(elem).width();
            maxHeight=$(element[i].parentElement).height();
            //alert('max height '+maxHeight+' max width '+maxWidth+' height '+overlayHeight+' width '+overlayWidth);

            var scaleX=maxWidth/overlayWidth;
            var scaleY=maxHeight/overlayHeight;
            var scale=scaleX+','+scaleY;
            $(elem).css({
            'transform-origin'  : 'top left',
            '-webkit-transform' : 'scale(' + scale + ')',
            '-moz-transform'    : 'scale(' + scale + ')',
            '-ms-transform'     : 'scale(' + scale + ')',
            '-o-transform'      : 'scale(' + scale + ')',
            'transform'         : 'scale(' + scale + ')'
            });
            clean(elem);
            i=i+1;
        }
        
    }

    function clean(element)
    {
        var link=element.getElementsByTagName('a');
        for(i=0;i<link.length;i++)
        {
            $(link[i]).hide();
        }
        var divs=element.getElementsByTagName('div');
        for(i=0;i<divs.length;i++)
        {
            $(divs[i]).css('border','');
        }
    }

    function checkForDataChange()
    {
        var playerId=document.getElementById('playerIdHolder').innerHTML;
        $('#loading').show();
        $.ajax({
            type:'POST',
            url:'/player/home/check',
            data:{
            _token: "{{ csrf_token() }}",
            player: playerId,
                },
            success:function(data){
                if(data=="true")
                    refreshData();
                else
                    $('#loading').fadeOut(300);
            },
            error: function(xhr, textStatus, errorThrown){
                $('#loading').fadeOut(300);
            }
        });
    }

    function refreshData()
    {
        var playerId=document.getElementById('playerIdHolder').innerHTML;
        $.ajax({
            type:'POST',
            async:true,
            url:'/player/home/refresh',
            data:{
            "_token": "{{ csrf_token() }}",
            "player": playerId,
                },
            success:function(data){
                document.getElementById('mainDiv').innerHTML=data;
                $('#loading').fadeOut(300);
                setTimeout(function() { reStart(); }, 1000);
                
            },
            error: function(xhr, textStatus, errorThrown){
            }
        });
    }

    function reStart()
    {
        clearTimeout(timeoutCall);
        slides=document.getElementById("slides").children;
        slidesNbr=slides.length;
        showingElement=0;
        if(slidesNbr>0)
            nextSlider();
        scaleOverlay();
    }

    /////////////////////////////////////////
    ///Artificial Intelegence////////////////
    /////////////////////////////////////////
  window.onload = function() {
    var isset=false;
    var snapping;
    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');
    var hiddenCanvas = document.getElementById('hidden-canvas');
    var context = canvas.getContext('2d');
    var hiddenContext = hiddenCanvas.getContext('2d');
    var take_photo_btn =document.getElementById('snapBtn')
    var image = document.querySelector('#snap');
    var tracker = new tracking.ObjectTracker('face');
    tracker.setInitialScale(4);
    tracker.setStepSize(1);
    tracker.setEdgesDensity(0.1);

    tracking.track('#video', tracker, { camera: true });

    tracker.on('track', function(event) {
      if(event.data.length===0)
      {
        context.clearRect(0, 0, canvas.width, canvas.height);
        if(isset)
        {
          clearTimeout(snapping);
          isset=false;
        }
      }
      else
      {
        event.data.forEach(function(rect) {
          context.clearRect(0, 0, canvas.width, canvas.height);
          context.strokeStyle = '#a64ceb';
          context.strokeRect(rect.x, rect.y, rect.width, rect.height);
          if(!isset)
          {
            snapping=setTimeout(function(){snap(image,hiddenCanvas,hiddenContext,video);},1000);
            isset=true;
          }
        });
      }
    });
  };

  function snap(image,canvas,context,video)
  {
    var snap = takeSnapshot(canvas,context);
    image.setAttribute('src', snap);
    image.classList.add("visible");
    $(canvas).css('z-index',-1);
    video.pause();
    sendImage(snap,canvas,video);
  }
  function takeSnapshot(canvas,context){
      var context = canvas.getContext('2d');

      var width = video.videoWidth;
      var height = video.videoHeight;

      if (width && height) {

          canvas.width = width;
          canvas.height = height;
          context.drawImage(video, 0, 0, width, height);
          return canvas.toDataURL('image/png');
      }
  }


  function sendImage(dataURL,canvas,video)
  {
    $.ajax({
          url: "/player/processimage",
          type: "POST",
          async:false,
          data: {
            _token: "{{ csrf_token() }}",
            imgBase64: dataURL
          },
          success: function (msg) {
              //alert("the image data : " +msg)
              video.play();
              insertRecievedAd(msg)
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); 
            alert("Error: " + errorThrown); 
            video.play();
          }  

      });
  }



</script>
<script src="{{ URL::asset('js/app.js') }}" charset="utf-8"></script>

@endsection
