@extends('layouts.unconfed')

@section('content')
<div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
        <div class="login-content">
            <div class="login-logo">
                <a href="index.html">
                    <img class="align-content" src="images/logo.png" alt="">
                </a>
            </div>
            @if($errors->any())
                <div class="alert alert-danger">
                    <p>{{$errors->first()}}</p>
                </div>
            @else
                <form id="myForm" method="post" action="{{action('PlayerController@login')}}">
                    @csrf
                </form>
            @endif
            
        </div>
    </div>
</div>
<script>
    document.getElementById('myForm').submit();
</script>
@endsection