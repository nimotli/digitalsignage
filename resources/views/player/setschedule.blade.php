@extends('layouts.app')

@section('content')

<div class=" container bg-light">
    <h3 class="my-3 text-center">Affecter un planning</h3>
    <div class="animated fadeIn">
        <div class="row p-3">
            <div class="col-8 py-3 offset-2">
                <form action="{{action('PlayerController@updateSchedule',$id)}}" method="post">
                    @csrf
                    <select class="form-control" name="schedule">
                        @foreach($schedules as $schedule)
                            <option value="{{$schedule->id}}">{{$schedule->name}}</option>
                        @endforeach
                    </select>
                    <button type="submit" class="btn btn-primary my-3">Affecter</button>
                </form>
            </div>
        </div>
        
    </div>
</div>

@endsection