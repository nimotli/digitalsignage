@extends('layouts.app')

@section('content')
    <div class="content mt-3">
        @if (\Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
        @endif
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Players Archivé</strong>
                            <a href="{{ url('/player') }}" class="float-right btn btn-primary">Players</a>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <!--<th>Id</th>-->
                                        <th>Nom</th>
                                        <th>Adresse</th>
                                        <th>Cpu</th>
                                        <th>Memoire</th>
                                        <!--<th>Marque ecran</th>-->
                                        <th>Type ecran</th>
                                        <th>Taille</th>
                                        <th>Resolution</th>
                                        <th>Ratio d'aspect</th>
                                        <th>Date mise en service</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($players as $player)
                                        @if ($player['archived'] == 1)
                                            <tr>
                                                <!--<td>{{$player['id']}}</td>-->
                                                <td>{{$player['nom']}}</td>
                                                <td>{{$player['adresse']}}</td>
                                                <td>{{$player['cpu']}}</td>
                                                <td>{{$player['memoir']}}</td>
                                                <!--<td>{{$player['marque']}}</td>-->
                                                <td>{{$player['type']}}</td>
                                                <td>{{$player['taille']}}</td>
                                                <td>{{$player['resolution']}}</td>
                                                <td>{{$player['ar']}}</td>
                                                <td>{{$player['datems']}}</td>
                                                <td>
                                                    <a href="{{action('PlayerController@accept', $player['id'])}}" class="btn btn-dark">Reassocier</a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection