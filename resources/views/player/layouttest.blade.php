@extends('layouts.unconfed')

@section('content')
<style>
    #adHolder{display: block !important;}
    .ai{
        position:absolute;z-index: 10000;bottom:0;right:0;width:175 ;height:175;;
      }
</style>

<div id="camHolder" style="z-index:-1:display:none;">
    <video class="ai" id="video" width="300" height="230" preload autoplay loop muted></video>
    <canvas class="ai" width="300" height="230" id="canvas" ></canvas>
    <canvas style="z-index:-3;" class="ai" width="350" height="350" id="hidden-canvas" ></canvas>
    <img style="display: none;" class="ai" id="snap">
</div>
<p style="display:none;" id="playerIdHolder">{{$playerId}}</p>
<div id="loading" style="display:none;position:fixed;height: 100vh;width: 100%;left:0;top:0;background-color:rgb(50,50,50);z-index: 10000;">
    <center><p style="font-size: 5em;left:35%;top:40%;position :fixed;color:#ccc;">LOADING...</p><center>
</div>

<div id="mainDiv" style="background-color:black;">
    {!! $data !!}
</div>

<script>
    var timeoutCall;
    var slides=document.getElementById("slides").children;
    var slidesNbr=slides.length;
    var showingElement=0;
    var showed=false;
    var maxHeight=$(window).height();
    var maxWidth=$(window).width();
    var tempElement;

    var isset=false;
    var snapping;
    var video;
    var canvas;
    var hiddenCanvas;
    var context;
    var hiddenContext;
    var image;
    var tracker;
    var issmart=false;
    var trackingTask;
    scaleOverlay();
    if(slidesNbr>0)
        nextSlider();
    function verifyValues()
    {
        if(showingElement<0)
            showingElement=0;
        if(showingElement>=slidesNbr)
            showingElement=0;
    }
    function callAfter(time)
    {
        timeoutCall = setTimeout(function() { nextSlider(); }, time);
    }
    function destroyAfter(element,time)
    {
        element.remove();
    }
    function nextSlider()
    {
        var overlayid = 'overlay'+slides[showingElement].getElementsByTagName("p")[1].innerHTML;
        if(slidesNbr>1)
        {
            var theIndex=-1;
            if(showingElement>0)
                theIndex=showingElement-1;
            else
                theIndex=slidesNbr-1;
            $(slides[showingElement]).css('z-index', 0);
            $(slides[showingElement]).show();
            $(slides[showingElement]).fadeTo(50,1);
            if(slides[theIndex].getElementsByTagName("p")[1].innerHTML!='temporary')
            {
                $(slides[theIndex]).css('z-index', 10);
                $(slides[theIndex]).fadeTo(500,0.01,function(){
                    $(slides[theIndex]).hide();
                });
            }
            if(slides[theIndex].getElementsByTagName("p")[1].innerHTML=='temporary'){
                destroyAfter(slides[theIndex]);
                slides=document.getElementById("slides").children;
                slidesNbr=slides.length;
                if(showingElement==0)
                    showingElement=slidesNbr-1;
                else
                    showingElement--;  
            }
            callAfter(parseFloat(slides[showingElement].getElementsByTagName("p")[0].innerHTML)*1000);
            if(showingElement==slidesNbr-1)
                showingElement=0;
            else
                showingElement++;            
        }
        else
        {
            $(slides[showingElement]).show();
            callAfter(parseFloat(slides[showingElement].getElementsByTagName("p")[0].innerHTML)*1000);
        }

        if(!$('#'+overlayid).is(":visible"))
        {
            var overlays=document.getElementsByClassName('overlays');
            for(j=0;j<overlays.length;j++)
            {
                if(overlayid!=overlays[j].id)
                {
                    if($(overlays[j]).is(":visible"))
                        $(overlays[j]).hide();
                }
            }
            $('#'+overlayid).show(100);
        }
    }

    
    function insertRecievedAd(ad)
    {
        ad=ad.split('-');
        var slidesHolder=document.getElementById("slides");
        var publicPath=ad[4];
        var filename=ad[1],filetype=ad[2],extractedData=JSON.parse(ad[3]);
        var itemToAdd="img";
        var elemHeight=$(slidesHolder.parentElement.innerHTML).height();
        var original,clone;

        if(filetype=='Video')
        {
            original = document.getElementById('vidOriginal');
            clone = original.cloneNode(true); 
            clone.id="";
            $(clone.getElementsByTagName('video')[0]).attr('src','/multimedia/'+filename);
        }
        else
        {
            original = document.getElementById('imgOriginal');
            clone = original.cloneNode(true); 
            clone.id="";
            $(clone.getElementsByTagName('img')[0]).attr('src','/multimedia/'+filename);
        }
        
        if(slidesNbr>0)
            slidesHolder.insertBefore(clone, slides[showingElement]);
        else
        {
            slidesHolder.appendChild(clone);
            nextSlider();
        }
        tempElement=clone;
        var smartDataHolder=tempElement.getElementsByClassName('smartData')[0];
        while (smartDataHolder.firstChild) 
        {
            smartDataHolder.removeChild(smartDataHolder.firstChild);
        }
        for(i = 0;i< extractedData.length;i++)
        {
            var t = document.createTextNode("Face "+(i+1)+"- Gender : "+extractedData[i].gender+" Emotion : "+extractedData[i].emotion); 
            var p = document.createElement("p");
            $(p).css('color','white');
            p.appendChild(t);
            smartDataHolder.appendChild(p);
        }

        slides=document.getElementById("slides").children;
        slidesNbr=slides.length;

    }

    ///////////////////////////////////
    //OVERLAY MANAGEMENET//////////////
    ///////////////////////////////////
    function scaleOverlay()
    {
        var element= document.getElementsByClassName('overlays');
        var i=0;
        while(i<element.length)
        {
            var elem=element[i].getElementsByTagName('div')[0];
            $(elem).css('z-index',5000);
            $(elem).css('position','fixed');
            var overlayHeight=$(elem).height();
            var overlayWidth=$(elem).width();
            maxHeight=$(element[i].parentElement).height();
            //alert('max height '+maxHeight+' max width '+maxWidth+' height '+overlayHeight+' width '+overlayWidth);

            var scaleX=maxWidth/overlayWidth;
            var scaleY=maxHeight/overlayHeight;
            var scale=scaleX+','+scaleY;
            $(elem).css({
            'transform-origin'  : 'top left',
            '-webkit-transform' : 'scale(' + scale + ')',
            '-moz-transform'    : 'scale(' + scale + ')',
            '-ms-transform'     : 'scale(' + scale + ')',
            '-o-transform'      : 'scale(' + scale + ')',
            'transform'         : 'scale(' + scale + ')'
            });
            clean(elem);
            i=i+1;
        }
        
    }

    function clean(element)
    {
        var link=element.getElementsByTagName('a');
        for(i=0;i<link.length;i++)
        {
            $(link[i]).hide();
        }
        var divs=element.getElementsByTagName('div');
        for(i=0;i<divs.length;i++)
        {
            $(divs[i]).css('border','');
        }
    }

    function checkForDataChange()
    {
        var playerId=document.getElementById('playerIdHolder').innerHTML;
        $('#loading').show();
        $.ajax({
            type:'POST',
            url:'/player/home/check',
            data:{
            _token: "{{ csrf_token() }}",
            player: playerId,
                },
            success:function(data){
                if(data=="true")
                    refreshData();
                else
                    $('#loading').fadeOut(300);
            },
            error: function(xhr, textStatus, errorThrown){
                $('#loading').fadeOut(300);
            }
        });
    }

    function refreshData()
    {
        var playerId=document.getElementById('playerIdHolder').innerHTML;
        $.ajax({
            type:'POST',
            async:true,
            url:'/player/home/refresh',
            data:{
            "_token": "{{ csrf_token() }}",
            "player": playerId,
                },
            success:function(data){
                document.getElementById('mainDiv').innerHTML=data;
                $('#loading').fadeOut(300);
                setTimeout(function() { reStart(); }, 4000);
                initialize();
            },
            error: function(xhr, textStatus, errorThrown){
            }
        });
    }

    function reStart()
    {
        clearTimeout(timeoutCall);
        slides=document.getElementById("slides").children;
        slidesNbr=slides.length;
        showingElement=0;
        if(slidesNbr>0)
            nextSlider();
        scaleOverlay();
    }

    /////////////////////////////////////////
    ///Artificial Intelegence////////////////
    /////////////////////////////////////////
  window.onload = function() {
    isset=false;
    snapping;
    video = document.getElementById('video');
    canvas = document.getElementById('canvas');
    hiddenCanvas = document.getElementById('hidden-canvas');
    context = canvas.getContext('2d');
    hiddenContext = hiddenCanvas.getContext('2d');
    image = document.querySelector('#snap');
    tracker = new tracking.ObjectTracker('face');
    issmart=false;
    tracker.setInitialScale(4);
    tracker.setStepSize(.5);
    tracker.setEdgesDensity(0.1);
    trackingTask = tracking.track('#video', tracker, { camera: true });
    initialize();

    tracker.on('track', function(event) {
        if(event.data.length===0)
        {
            context.clearRect(0, 0, canvas.width, canvas.height);
            if(isset)
            {
            clearTimeout(snapping);
            isset=false;
            }
        }
        else
        {
            if(issmart)
            {
                context.clearRect(0, 0, canvas.width, canvas.height);
                event.data.forEach(function(rect) {
                    context.strokeStyle = '#a64ceb';
                    context.strokeRect(rect.x, rect.y, rect.width, rect.height);
                    if(!isset)
                    {
                        snapping=setTimeout(function(){snap(image,hiddenCanvas,hiddenContext,video);},1000);
                        isset=true;
                    }
                });
            }
        }
    });

  };

  function initialize()
  {
    var vidParent=document.getElementById('video').parentElement.children;
    if(document.getElementById('pageConfig').innerHTML=='smart')
        issmart=true;
    else
        issmart=false;
    if(issmart)
    {
        $(vidParent[0]).css('z-index','10000');
        $(vidParent[1]).css('z-index','10000');
    }
    else
    {
        $(vidParent[0]).css('z-index','-1');
        $(vidParent[1]).css('z-index','-1');
    }
  }

  function snap(image,canvas,context,video)
  {
    var snap = takeSnapshot(canvas,context);
    image.setAttribute('src', snap);
    image.classList.add("visible");
    $(canvas).css('z-index',-1);
    video.pause();
    sendImage(snap,canvas,video);
  }
  function takeSnapshot(canvas,context){
      var context = canvas.getContext('2d');

      var width = video.videoWidth;
      var height = video.videoHeight;

      if (width && height) {

          canvas.width = width;
          canvas.height = height;
          context.drawImage(video, 0, 0, width, height);
          return canvas.toDataURL('image/png');
      }
  }


  function sendImage(dataURL,canvas,video)
  {
    var playerId=document.getElementById('playerIdHolder').innerHTML;
    $.ajax({
          url: "/player/processimage",
          type: "POST",
          data: {
            _token: "{{ csrf_token() }}",
            imgBase64: dataURL,
            player:playerId,
          },
          success: function (msg) {
              //alert("the image data : " +msg)
              video.play();
              if(msg!="nothingFound")
                insertRecievedAd(msg)
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); 
            alert("Error: " + errorThrown); 
            video.play();
          }  

      });
  }



</script>
<script src="{{ URL::asset('js/app.js') }}" charset="utf-8"></script>

@endsection
