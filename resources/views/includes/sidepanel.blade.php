<?php
    $ar=\App\Http\Controllers\Auth\LoginController::getAccessRights();
    $isadmin=false;
    if($ar == "admin")
        $isadmin=true;
?>
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img src="images/logos.png" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./"><img src="images/logo2s.png" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="/dashboard"> <i class="menu-icon fa fa-dashboard"></i>Tableau de bords </a>
                </li>
                @if($isadmin)
                    <h3 class="menu-title">Application</h3><!-- /.menu-title -->
                    <li>
                        <a href="{{url('utilisateurs')}}"> <i class="menu-icon fa fa-users"></i>Utilisateurs </a>
                    </li>
                    <li>
                        <a href="{{url('player')}}"> <i class="menu-icon fa fa-tablet"></i>Players </a>
                    </li>
                    <li>
                        <a href="{{url('groups')}}"> <i class="menu-icon fa fa-qrcode"></i>Groupes </a>
                    </li>
                    <h3 class="menu-title">Annonces</h3><!-- /.menu-title -->
                    <li>
                        <a href="{{url('multimedias')}}"> <i class="menu-icon fa fa-youtube-play"></i>Multimedia </a>
                    </li>
                    <li>
                        <a href="{{url('annonces')}}"> <i class="menu-icon fa fa-bullhorn"></i>Annonces personnalisés </a>
                    </li>
                    <li>
                        <a href="{{url('sequences')}}"> <i class="menu-icon fa fa-sitemap"></i>Sequence </a>
                    </li>
                    <li>
                        <a href="{{url('schedules')}}"> <i class="menu-icon fa fa-calendar"></i>Planning </a>
                    </li>
                    <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
                    <li>
                        <a href="{{url('/part')}}"> <i class="menu-icon fa fa-th"></i>Segments du player</a>
                    </li>
                    <li>
                        <a href="{{url('/parameter')}}"> <i class="menu-icon fa fa-gear"></i>Paramètres </a>
                    </li>
                @else
                    <h3 class="menu-title">Application</h3><!-- /.menu-title -->
                    @foreach($ar as $a)
                        @if($a->name=="utilisateur" || $isadmin)
                            <li>
                                <a href="{{url('utilisateurs')}}"> <i class="menu-icon fa fa-users"></i>Utilisateurs </a>
                            </li>
                            @break
                        @endif
                    @endforeach
                    @foreach($ar as $a)
                        @if($a->name=="player" || $isadmin)
                            <li>
                                <a href="{{url('player')}}"> <i class="menu-icon fa fa-tablet"></i>Players </a>
                            </li>
                            @break
                        @endif
                    @endforeach
                    @foreach($ar as $a)
                        @if($a->name=="group" || $isadmin)
                            <li>
                                <a href="{{url('groups')}}"> <i class="menu-icon fa fa-qrcode"></i>Groupes </a>
                            </li>
                            @break
                        @endif
                    @endforeach
                
                    <h3 class="menu-title">Annonces</h3><!-- /.menu-title -->
                    @foreach($ar as $a)
                        @if($a->name=="multimedia" || $isadmin)
                            <li>
                                <a href="{{url('multimedias')}}"> <i class="menu-icon fa fa-youtube-play"></i>Multimedia </a>
                            </li>
                            @break
                        @endif
                    @endforeach
                    @foreach($ar as $a)
                        @if($a->name=="annonce" || $isadmin)
                            <li>
                                <a href="{{url('annonces')}}"> <i class="menu-icon fa fa-bullhorn"></i>Annonces personnalisés </a>
                            </li>
                            @break
                        @endif
                    @endforeach
                    @foreach($ar as $a)
                        @if($a->name=="sequence" || $isadmin)
                            <li>
                                <a href="{{url('sequences')}}"> <i class="menu-icon fa fa-sitemap"></i>Sequence </a>
                            </li>
                            @break
                        @endif
                    @endforeach
                    @foreach($ar as $a)
                        @if($a->name=="schedule" || $isadmin)
                            <li>
                                <a href="{{url('schedules')}}"> <i class="menu-icon fa fa-calendar"></i>Planning </a>
                            </li>
                            @break
                        @endif
                    @endforeach
                    <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
                    <li>
                        <a href="{{url('/part')}}"> <i class="menu-icon fa fa-th"></i>Segments du player</a>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>