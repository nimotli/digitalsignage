<header id="header" class="header">

        <div class="header-menu">

            <div class="col-sm-7">
                <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                <div class="header-left">
                    <div class="form-inline">
                        
                    </div>
                </div>
            </div>

            <div class="col-sm-5">

                <div class="user-area dropdown float-right">
                    @guest
                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                        <a href="{{ route('register') }}">{{ __('Register') }}</a>
                    @else
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="http://localhost:8000/images/admin.jpg" alt=" ">
                        </a>
    
                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="{{url('/parameter')}}"><i class="fa fa-cog"></i>  Paramètres
                                </a>
    
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i>  Deconnexion
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </div>
                    </div>
                    @endguest
                </div>
            </div>
        </div>
    </header>