@extends('layouts.app')

@section('content')
<div class="content mt-3">
    @if (\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Table</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Player</th>
                                    <th>Part</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $dpart)
                                <tr>
                                    <td>{{$dpart['id']}}</td>
                                    <td>{{$dpart['player']}}</td>
                                    <td>{{$dpart['part']}}</td>
                                    <td><a href="{{action('PlayerController@editPart', $dpart['id'])}}" class="btn btn-dark">modifier</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection