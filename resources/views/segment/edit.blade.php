@extends('layouts.app')

@section('content')

<div class="container">
    <h2>Modifier  </h2><br/>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div id="restEl">
        <div class="row p-4 my-3 bg-light border" style="height:300px; overflow-y:scroll;">
            @foreach($multimedias as $mult)
                @if($mult->uploader==$aId || $mult->type=="public")
                    <div class="col-md-2">
                        <div class="card text-center">
                            <div class="card-header text-muted">
                                    {{$mult->media_type}} | {{$mult->type}}
                            </div>
                            <div class="card-body" onclick="select(this)" id="{{$mult->id}}" @if($mult->id==$dp->multimedia) style="border:2px double royalblue;"@endif>
                                @if($mult->media_type=='Video')
                                <video src="{{ asset('multimedia/'.$mult->filename)}}" class="embed-responsive embed-responsive-16by9" id="{{$mult->id}}" ></video>
                                @else
                                <img src="{{ asset('multimedia/'.$mult->filename)}}" class="multImages" id="{{$mult->id}}" >
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <form action="{{action('PlayerController@updatePart', $id)}}" method="POST">
            @csrf
            <input type="hidden" name="multimedia" id="mult">
            <center><button type="submit" class="btn btn-primary">Modifier</button></center>
        </form>
    </div>
</div>
<script>
    var selectedMult=null;

    function select(target)
    {
        if(selectedMult!=null)
        {
            $(selectedMult).css('border','');
        }
        else
        {
            var elems=document.getElementsByClassName('card-body');
            for(i=0;i<elems.length;i++)
            {
                $(elems[i]).css('border','');
            }
        }
        selectedMult=target;
        $('#mult').val(target.id);
        $(selectedMult).css('border','2px double royalblue');
    }
</script>
@endsection