@extends('layouts.app')

@section('content')
<div class="container bg-light my-5 py-3">
    <h2>Parametres du serveur </h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('ServerconfigController@update')}}">
            @csrf
            {{csrf_field()}}
            <div class="container border">
                <br>
                <h3>Information d'application</h3>
                <br>
                <div class="form-group">
                    <label for="token">Code Serveur</label>
                    <input type="text" class="form-control" id="token" name="token" value="{{$sc->token}}" >
                    <small id="passwordHelpInline" class="text-muted">
                        Utilisé pour connecter les player au serveur.
                      </small>
                </div>
            </div>
            <br>
            <div class="container border">
                <br>
                <h3>Information de client</h3>
                <br>
                <div class="form-group">
                    <label for="rs">Raison social</label>
                    <input type="text" class="form-control" id="rs" name="rs" value="{{$sc->rs}}" >
                </div>
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" value="{{$sc->nom}}" >
                </div>
                <div class="form-group">
                    <label for="prenom">Prenom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" value="{{$sc->prenom}}" >
                </div>
                <div class="form-group">
                    <label for="phon">Telephone</label>
                    <input type="text" class="form-control" id="phon" name="phon" value="{{$sc->telephon}}" >
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{$sc->email}}" >
                </div>
            </div>
            <br>
            <div class="container border">
                <br>
                <h3>Information d'admin</h3>
                <br>
                <div class="form-group">
                    <label for="emaila">Email</label>
                    <input type="text" class="form-control" id="emaila" name="emaila" value="{{$sc->emaila}}" >
                </div>
                <div class="form-group">
                    <label for="oldpassword">Mot de passe actuel</label>
                    <input type="password" class="form-control" id="oldpassword" name="oldpassword" placeholder="Mot de passe" >
                </div>
                <div class="form-group">
                    <label for="password">Nouveau mot de passe</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" >
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm mot de passe</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Mot de passe" >
                </div>
                <div class="form-group">
                    <label for="noma">Nom</label>
                    <input type="text" class="form-control" id="noma" name="noma" value="{{$sc->noma}}" >
                </div>
                <div class="form-group">
                    <label for="prenoma">Prenom</label>
                    <input type="text" class="form-control" id="prenoma" name="prenoma" value="{{$sc->prenoma}}" >
                </div>
                <div class="form-group">
                    <label for="phona">Telephon</label>
                    <input type="text" class="form-control" id="phona" name="phona" value="{{$sc->telephona}}" >
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Valider</button>
        </form>
</div>
   
@endsection