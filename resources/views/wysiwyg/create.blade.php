@extends('layouts.app')

@section('content')

<style>
.draggable
{
    position: absolute;
    height: 150px;
    width: 150px;
    display: none;
}
#adHolder
{
    border: 1px solid black;
    background-color: seagreen;
    display: block !important;
} 
</style>
    <div class="container-fluid">
        <div class="m-3 row border border-secondary bg-light p-3">
            <div class="col">
                <form method="post" action="{{url('annonces')}}" id="myForm">
                    @csrf
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="nom">Nomm</label>
                        <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" required>
                        <input type="hidden" name="html" id="htmlHolder">
                    </div>
                    <button onclick="sendForm(event)" class="btn btn-primary mb-2">Cree</button>
                </form>
            </div>
            
        </div>
        <div class="m-3 row border border-secondary bg-light" >
            <div class="col-md-3 p-3 border">
                <label for="ar">Ratio d'aspect</label>
                <select class="form-control" id="ar">
                    <option value="16x9" selected>16x9</option>
                    <option value="4x3">4x3</option>
                    <option value="16x10">16x10</option>
                </select>
                <button class="btn btn-primary mt-1" id="regionAdder">Ajouter region</button>
            </div>
            <div class="col-9 bg-dark" id="forhide">
    
            </div>
    
             <div class="col-9" id="forshow" style="display: none;">
                <div class="col-md-2">
                    <label for="wdt">Width</label>
                    <input type="text" class="form-control" id="wdt" readonly>
                    <label for="hgt">Height</label>
                    <input type="text" class="form-control" id="hgt" readonly>
                </div>
                <div class="col-md-2 border-right">
                    <label for="lft">left</label>
                    <input type="text" class="form-control" id="lft" readonly>
                    <label for="tp">Top</label>
                    <input type="text" class="form-control" id="tp" readonly>
                </div>
                <div class="col-md-4" id="txtAffect" style="display:none;">
                    <label for="txt">text</label>
                    <textarea class="form-control" id="txt"></textarea>
                    <button class="btn btn-secondary form-control" onclick="validateText()">valider</button>
                </div>
                <div class="col-md-4" id="textPar" style="display:none;">
                    <label for="txtalign">align</label>
                    <select class="form-control" id="txtalign">
                        <option value="0">gauche</option>
                        <option value="1">centre</option>
                        <option value="2">droit</option>
                    </select>
                    <label for="txtclr">Couleur text</label>
                    <input type="color" class="form-control" id="txtclr">
                    <label for="autofit">Autofit ?</label>
                    <input type="checkbox" id="autofit" checked>
                    <div id="sizeDiv" style="display: none;">
                        <label for="autofit">Size</label>
                        <input class="form-control" type="number" id="txtsize" min=0>
                    </div>
                </div>
                <div class="col-md-4" id="imagePar" style="display:none;">
                    <label for="txt">text</label>
                    <textarea class="form-control" id="txt"></textarea>
                    <button class="btn btn-secondary form-control" onclick="validateText()">valider</button>
                </div>
                <div class="col-md-4" id="imgAffect" style="display:none;overflow-y: scroll;max-height: 200px;">
                    <label for="imgg">Images</label>
                    @foreach($multimedias as $mult)
                        @if($mult->media_type=="Image")
                            <div class="col-md-12">
                                <img src="{{ asset('multimedia/'.$mult->filename)}}" onclick="selectImage(this)">
                            </div>
                        @endif
                    @endforeach                 
                    <button class="btn btn-secondary form-control" onclick="validateImage()">valider</button>
                </div>
                <div class="col-md-4" id="noAffect" >
                </div>
                <div class="col-md-4 border-left">
                    <button class="btn btn-primary mt-1 form-control mb-1" onclick="affectText()">Affecter text</button>
                    <button class="btn btn-primary mt-1 form-control mb-1" onclick="affectImage()">Affecter image</button>
                    <label for="transp">Transparent ?</label>
                    <input type="checkbox" id="transp"><br>
                    <div id="clrDiv">
                        <label for="clr">Couleur region</label>
                        <input type="color" class="form-control" id="clr">
                    </div>
                 </div>
                
             </div>
        </div>
        <div id="adHolder"  unselectable="on" onselectstart="return false;">  
            
        </div>
    </div>
    <div onclick="select(this)" class="draggable" id="original" style="border: 1px solid grey;">
        <a href="#" onclick="removeRegion(this,event)" onmouseup="fit(this)" style="z-index:10;"><i class="fa fa-times float-right"></i></a>
        <p style="display:none;">none</p>
    </div>
<script>

    //var selectedRegion=null;
    var autofit=true;
    var selectedImage=null;
    var o = { 
        selectedRegion: null,
        get b() 
        { 
            return this.selectedRegion;
        },
        set c(x)
        {
            if(x==null)
            {
                $('#forhide').show();
                $('#forshow').hide();
            }
            else
            {
                $('#forhide').hide();
                $('#forshow').show();
            }
            this.selectedRegion = x;
        }
     };

    function affectText()
    {
        if(o.b!=null)
        {
            $('#imgAffect').hide();
            $('#txtAffect').show();
            $('#noAffect').hide();
            $('#textPar').hide();
            $('#imgPar').hide();
        }
    }
    function affectImage()
    {
        if(o.b!=null)
        {
            $('#imgAffect').show();
            $('#txtAffect').hide();
            $('#noAffect').hide();
            $('#textPar').hide();
            $('#imgPar').hide();
        }
    }
    function showImageParams()
    {
        if(o.b!=null)
        {
            $('#imgAffect').hide();
            $('#txtAffect').hide();
            $('#noAffect').hide();
            $('#textPar').hide();
            $('#imgPar').show();
        }
    }
    function showTextParams()
    {
        $('#imgAffect').hide();
        $('#txtAffect').hide();
        $('#noAffect').hide();
        $('#textPar').show();
        $('#imgPar').hide();
    }
    function affectHide()
    {
        $('#imgAffect').hide();
        $('#txtAffect').hide();
        $('#noAffect').show();
        $('#textPar').hide();
        $('#imgPar').hide();
    }
    
    function validateText()
    {
        o.b.getElementsByTagName('p')[0].innerHTML="text";
        var otherDiv=o.b.getElementsByTagName('div');
        if(otherDiv.length>0)
        {
            otherDiv[0].parentNode.removeChild(otherDiv[0]);
        }
        var div = document.createElement("DIV");
        var t = document.createTextNode($('#txt').val()); 
        div.appendChild(t); 
        o.b.appendChild(div);
        fit(o.b);
        showTextParams();
        syncTextParams();
    }
    function validateImage()
    {
        $(o.b).css("background-image", 'url('+selectedImage.src+')');  
        $(o.b).css("background-size", 'cover');  
        o.b.getElementsByTagName('p')[0].innerHTML="image"
        affectHide();
    }
    function fit(element)
    {
        if(autofit)
        {
            var has=element.getElementsByTagName('p')[0].innerHTML;
            if(has=="text")
                fitText(element.getElementsByTagName('div')[0]);
        }
    }
    function fitText(element) 
    {
        var entered=false;
        while( $(element).height() > $(element.parentNode).height()+10 ) 
        {
            $(element).css('font-size', (parseInt($(element).css('font-size')) - 1) + "px" );
            if(!entered)entered=true;
        }
        if(entered)return;
        while( $(element).height() < $(element.parentNode).height()-10) 
        {
            $(element).css('font-size', (parseInt($(element).css('font-size')) + 1) + "px" );
            if(!entered)entered=true;
        }
        if(entered)return;
    }
    function syncTextParams()
    {
        var element=o.b.getElementsByTagName('div')[0];
        var align=$(element).css('text-align');
        if(align=="right")
            $('#txtalign').val("2");
        else if(align=="center")
            $('#txtalign').val("1");
        else if(align=="left")
        {
            $('#txtalign').val("0");
        }

        var rgb= rgbFromString($(element).css('color'));
        var hex=rgb2hex(rgb);
        $('#txtclr').val(hex);
    }
    function select(element)
    {
        if(o.b!=null)
        {
            $(o.selectedRegion).css('border','1px solid grey');
            $(o.selectedRegion).css('z-index',0);
        }
        o.c=element;
        if(element!=null)
        {
            $(element).css('border','1px dashed white');
            $(element).css('z-index',10);
            var hex=rgb2hex(rgbFromString($(element).css('backgroundColor')));
            $('#clr').val(hex);
            if(element.getElementsByTagName('p')[0].innerHTML=="text")
            {
                
                syncTextParams();
                showTextParams();
                syncTextSize();
            }
            else if(element.getElementsByTagName('p')[0].innerHTML=="image")
                affectHide();//showImageParams();
            else
                affectHide();
        }
    }
    function selectImage(target)
    {
        if(selectedImage!=null)
        {
            $(selectedImage).css('border','');
        }
        selectedImage=target;
        $(selectedImage).css('border','3px double royalblue');
    }
    function syncTextSize()
    {
        var element = o.b.getElementsByTagName('div')[0];
        var size = $(element).css('font-size').split('p')[0];
        $('#txtsize').val(size);
    }

    function sendForm(ev)
    {
        ev.preventDefault();
        var form=$('#myForm');
        showAdHtml();
        form.submit();
    }

    $( document ).ready(function(){
        $("#ar").val('16x9');
        $('#adHolder').height(getPxfromPercent($('#adHolder').width(),56.25));
    });


    $('#txtsize').on('change',function(){
        if(!autofit)
        {
            
            var element =o.b.getElementsByTagName('div')[0];
            var fontsize=$(this).val()+"px";
            $(element).css('font-size',fontsize);
        }
    });

    $('#txtalign').on('change',function(){
        var element=o.b.getElementsByTagName('div')[0];
        if($(this).val()==0)
            $(element).css('text-align','left');
        if($(this).val()==1)
            $(element).css('text-align','center');
        if($(this).val()==2)
            $(element).css('text-align','right');
    })
    $('#txtclr').on('change',function(){
        var element = o.b.getElementsByTagName('div')[0];
        $(element).css('color',$('#txtclr').val());
    })

    $('#autofit').on('change',function(){
        if($(this).is(":checked"))
        {
            autofit=true;
            $('#sizeDiv').hide(100);
            syncTextSize();
            fit(o.b);
        }
        else
        {
            autofit=false;
            $('#sizeDiv').show(100);
            syncTextSize();
        }
    })

    $('#transp').on('change',function(){
        if($(this).is(":checked"))
        {
            autofit=true;
            $('#clrDiv').hide(100);
            $(o.b).css('background-color','transparent');
        }
        else
        {
            autofit=false;
            $('#clrDiv').show(100);
            $(o.b).css('background-color','rgba(0,0,0,.7)');

        }
    })

    $('#regionAdder').on('click',function(){
        var original=document.getElementById('original');
        var clone = original.cloneNode(true);
        clone.id="";
        clone.style.display="block";
        var r=Math.floor(Math.random() * 256);
        var g=Math.floor(Math.random() * 150);
        var b=Math.floor(Math.random() * 256);
        clone.style.backgroundColor="rgba("+r+","+g+","+b+",.7)";
        
        document.getElementById('adHolder').insertBefore(clone,document.getElementById('adHolder').firstChild);
    })

    $('#ar').change(function(){
        
        if(this.value=="16x9")
        {
            var newHeight=getPxfromPercent($('#adHolder').width(),56.25);
            $('#adHolder').height(newHeight);
        }
        else if(this.value=="4x3")
        {
            var newHeight=getPxfromPercent($('#adHolder').width(),75);
            $('#adHolder').height(newHeight);
        }
        else
        {
            var newHeight=getPxfromPercent($('#adHolder').width(),62.5);
            $('#adHolder').height(newHeight);
        }
    })
    $('#clr').change(function(){
        $(o.b).css('background-color',$('#clr').val());
        var rgb = $(o.b).css('background-color');
        var rgba =rgbFromString(rgb)
        var newRgba='rgba('+rgba[0]+','+rgba[1]+','+rgba[2]+','+'0.7)';
        $(o.b).css('background-color',newRgba);
    })

///////////////////////////////////////////////////////////////
///Utility functions                                        ///
///////////////////////////////////////////////////////////////

    function getPxfromPercent(width,percent)
    {
        var px=0;
        px=(width*percent)/100;
        return px;
    }

    function removeRegion(element,ev)
    {
        ev.preventDefault();
        element.parentNode.parentNode.removeChild(element.parentNode);
    }

    function rgbFromString(str)
    {
        var raw=str.split('(')[1].split(')')[0];
        var rgb=raw.split(',');
        return rgb;
    }
    function rgb2hex(rgb)
    {
        //rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        function hex(x)
        {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return "#" + hex(rgb[0]) + hex(rgb[1]) + hex(rgb[2]);
    }

    function showAdHtml()
    {
        var element=document.getElementById('adHolder');
        var width=$(element).width();
        $(element).width(width);
        
        var divs=element.getElementsByTagName('div');
        for(i=0;i<divs.length;i++)
        {
            $(divs[i]).css('border','1px solid grey');
            if($(divs[i]).attr('class')=="draggable")
            {
                var tempwidth=$(divs[i]).width();
                var tempheight=$(divs[i]).height();
                $(divs[i]).css('position','absolute');
                $(divs[i]).height(tempheight);
                $(divs[i]).width(tempwidth);
            }
        }
        var htmlCode = element.outerHTML;
        $('#htmlHolder').val(htmlCode);
    }
///////////////////////////////////////////////////////////////
///Drag and resize code                                     ///
///////////////////////////////////////////////////////////////

    interact('.draggable')
  .draggable({
    inertia: false,
    restrict: {
      restriction: "parent",
      endOnly: false,
      elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    },
    autoScroll: true,

    onmove: dragMoveListener,
   
  }).resizable({
    edges: { left: true, right: true, bottom: true, top: true },

    restrictEdges: {
      outer: 'parent',
      endOnly: false,
    },

    restrictSize: {
      min: { width: 100, height: 50 },
    },

    inertia: true,
  })
  .on('resizemove', function (event) {
    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0),
        y = (parseFloat(target.getAttribute('data-y')) || 0);

    target.style.width  = event.rect.width + 'px';
    target.style.height = event.rect.height + 'px';

    x += event.deltaRect.left;
    y += event.deltaRect.top;

    target.style.webkitTransform = target.style.transform =
        'translate(' + x + 'px,' + y + 'px)';

    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    $('#hgt').val(Math.round(event.rect.height));
    $('#wdt').val(Math.round(event.rect.width));
    fit(target);
  });
  function dragMoveListener (event) {
    var target = event.target,
    x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    $('#lft').val(x+1);
    $('#tp').val(y+1);
  }
  window.dragMoveListener = dragMoveListener;

</script>
@endsection
