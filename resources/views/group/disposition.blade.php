@extends('layouts.app')

@section('content')
    <div class="container bg-light my-5 py-3" >
        <h2>Disposition group </h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('GroupController@updateDispo',$id)}}">
            @csrf
            <div class="container border">
                <div class=" mx-auto">
                    <select name="" id="layout">
                        <option data-img-class="first" data-img-alt="Séquence seule" value="0" data-img-src="{{ asset('images/Sequence-Seul.png') }}">Default</option>
                        <option data-img-alt="Séquence avec une bande avec texte défilant" value="1" data-img-src="{{ asset('images/Sequence-Seul-Bar.png') }}">Single bar</option>
                        <option data-img-alt="Séquence avec deux bandes" value="2" data-img-src="{{ asset('images/Sequence-Seul-Deux-Bar.png') }}">Two bars</option>
                        <option data-img-alt="Quatre séquences publicitaires simultanément" value="3" data-img-src="{{ asset('images/Sequence-Quatres.png') }}">Four sequences</option>
                     
                    </select>
                </div>
                <div class="form-group" id="pos">
                    <label for="pos">Position bande</label>
                    <select name="pos" id="pos" class="form-control">
                        <option value="0">top</option>
                        <option value="1">bot</option>
                    </select>
                </div>
                <div class="form-group" id="txt1">
                    <label for="topbar">Text bande</label>
                    <input type="text" name="txt1" id="topbar" class="form-control" >
                </div>
                <div class="form-group" id="txt2">
                    <label for="botbar">Text deuxieme bande</label>
                    <input type="text" name="txt2" id="botbar" class="form-control" >
                </div>
                <input type="hidden" name="type" id="type" >
                    
            </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Modifier</button>
        </form>
    </div>
    <script>

        $("#layout").imagepicker({
            show_label: true
            });
            $('#pos').hide();
            $('#txt1').hide();
            $('#txt2').hide();

            $('#layout').on('change', function() {
                if( this.value == 1)
                {
                    $('#pos').show(300);
                    $('#txt1').show(300);
                    $('#txt2').hide(300);
                    $('#type').val(1);
                }
                else if(this.value == 2)
                {
                    $('#pos').hide(300);
                    $('#txt1').show(300);
                    $('#txt2').show(300);
                    $('#type').val(2);
                }
                else if(this.value == 0)
                {
                    $('#pos').hide(300);
                    $('#txt1').hide(300);
                    $('#txt2').hide(300);
                    $('#type').val(0);
                }
                else
                {
                    $('#pos').hide(300);
                    $('#txt1').hide(300);
                    $('#txt2').hide(300);
                    $('#type').val(4);
                }
            });
        $( document ).ready(function() {
            var slctedval = $('#layout').val();
            if(slctedval==1)
            {
                $('#pos').show(300);
                $('#txt1').show(300);
                $('#txt2').hide(300);
                $('#type').val(1);
            }
            else if(slctedval==2)
            {
                $('#pos').hide(300);
                $('#txt1').show(300);
                $('#txt2').show(300);
                $('#type').val(2);
            }
            else if(slctedval==0)
            {
                $('#pos').hide(300);
                $('#txt1').hide(300);
                $('#txt2').hide(300);
                $('#type').val(0);
            }
            else 
            {
                $('#pos').hide(300);
                $('#txt1').hide(300);
                $('#txt2').hide(300);
                $('#type').val(4);
            }
        });
    </script>
@endsection