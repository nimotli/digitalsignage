@extends('layouts.app')

@section('content')
    <div class="container">
    <h2>Modifier un utilisateur </h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('UtilisateurController@update', $id)}}">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <div class="form-group">
                    <label for="email">Addresse email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{$utilisateur->email}}" >
            </div>
            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" id="nom" name="nom" value="{{$utilisateur->nom}}" >
            </div>
            <div class="form-group">
                    <label for="prenom">Prenom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" value="{{$utilisateur->prenom}}" >
            </div>
            <div class="form-group">
                <label for="phon">Telephon</label>
                <input type="text" class="form-control" id="phon" name="phon" value="{{$utilisateur->telephon}}" >
            </div>
            <button type="submit" class="btn btn-primary mb-2">Modifier</button>
        </form>
    </div>
   
@endsection