@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Creer un utilisateur</h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{url('utilisateurs')}}">
            @csrf
            <div class="form-group">
                    <label for="email">Addresse email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" >
            </div>
            <div class="form-group">
                <label for="password">Mot de passe</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" >
            </div>
            <div class="form-group">
                    <label for="password_confirmation">Confirme mot de passe</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm motdepasse" >
            </div>
            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" >
            </div>
            <div class="form-group">
                    <label for="prenom">Prenom</label>
                    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prenom" >
            </div>
            <div class="form-group">
                <label for="phon">Telephon</label>
                <input type="text" class="form-control" id="phon" name="phon" placeholder="+21265987465" >
            </div>
            <button type="submit" class="btn btn-primary mb-2">Creer</button>
        </form>
    </div>
   
@endsection