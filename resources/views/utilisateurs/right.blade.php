@extends('layouts.app')

@section('content')
    <div class="container bg-light my-5 py-3 border" >
        <h2>Gestion des droits d'acces</h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{action('UtilisateurController@updateRights',$id)}}" method="post" id="myFrom">
            @csrf
            {{csrf_field()}}
                        <center style="text-align: left;">
                            <div class="row  border-top p-2">
                                <input type="checkbox" id="utilisateur" class="col-1 offset-2 rights"
                            @foreach($rights as $right)@if($right->name=='utilisateur')checked @endif @endforeach> 
                                <span class="col-4">Gestion des utilisateurs</span> 
                            </div>
                            <div class="row border-top p-2">
                                <input type="checkbox" id="player" class="col-1 offset-2 rights"
                                @foreach($rights as $right) @if($right->name=='player')checked @endif @endforeach> 
                                <span class="col-4">Gestion des players</span> 
                            </div>
                            <div class="row border-top p-2">
                                <input type="checkbox" id="group" class="col-1 offset-2 rights"
                                @foreach($rights as $right) @if($right->name=='group')checked @endif @endforeach> 
                                <span class="col-4">Gestion des groupes</span> 
                            </div>
                            <div class="row border-top p-2">
                                <input type="checkbox" id="multimedia" class="col-1 offset-2 rights"
                                @foreach($rights as $right) @if($right->name=='multimedia')checked @endif @endforeach> 
                                <span class="col-4">Gestion des multimedias</span> 
                            </div>
                            <div class="row border-top p-2">
                                <input type="checkbox" id="annonce" class="col-1 offset-2 rights"
                                @foreach($rights as $right) @if($right->name=='annonce')checked @endif @endforeach> 
                                <span class="col-4">Gestion des annonces personnalise</span> 
                            </div>
                            <div class="row border-top p-2">
                                <input type="checkbox" id="sequence" class="col-1 offset-2 rights"
                                @foreach($rights as $right) @if($right->name=='sequence')checked @endif @endforeach> 
                                <span class="col-4">Gestion des sequences</span> 
                            </div>
                            <div class="row border p-2">
                                <input type="checkbox" id="schedule" class="col-1 offset-2 rights"
                                @foreach($rights as $right) @if($right->name=='schedule')checked @endif @endforeach> 
                                <span class="col-4">Gestion des plannings</span> 
                            </div>
                        </center>

                    <br>
                    <input type="hidden" id="data" name="data" >
            <button onclick="send(event)" class="btn btn-primary mb-2">Modifier</button>
        </form>
    </div>

    <script>
    function send(ev)
    {
        ev.preventDefault();
        var form = document.getElementById('myFrom');
        var chks=document.getElementsByClassName('rights');
        var dataString="";
        for(i=0;i<chks.length;i++)
        {
            if($(chks[i]).is(":checked"))
            {
                if(dataString=="")
                    dataString=chks[i].id;
                else
                    dataString+="-"+chks[i].id;
            }
        }
        document.getElementById('data').value=dataString;
        form.submit();
    }
    </script>
@endsection