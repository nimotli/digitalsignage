@extends('layouts.app')

@section('content')
<div class="content mt-3">
    @if (\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Table</strong>
                        <a href="{{ route('utilisateurs.create') }}" class="float-right btn btn-primary">Creer utilisateur</a>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>Email</th>
                                    <th>Telephon</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($utilisateurs as $utilisateur)
                                <tr>
                                    <td>{{$utilisateur['id']}}</td>
                                    <td>{{$utilisateur['nom']}}</td>
                                    <td>{{$utilisateur['prenom']}}</td>
                                    <td>{{$utilisateur['email']}}</td>
                                    <td>{{$utilisateur['telephon']}}</td>
                                    <td><a href="{{action('UtilisateurController@rights', $utilisateur['id'])}}" class="btn btn-dark">Droits</a></td>
                                    <td><a href="{{action('UtilisateurController@edit', $utilisateur['id'])}}" class="btn btn-warning">Modifier</a></td>
                                    <td>
                                    <form action="{{action('UtilisateurController@destroy', $utilisateur['id'])}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger" type="submit">Supprimer</button>
                                    </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection