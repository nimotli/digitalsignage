@extends('layouts.app')

@section('content')
    <div id="app">
        <p>this is an event listener page and when the event is fired off this page will listen to it</p>
    </div>
    <script src="js/app.js" charset="utf-8"></script>
@endsection