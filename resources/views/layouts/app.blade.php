<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.header')
</head>
<body>
        @guest
            @yield('content')
        @else
            @include('includes.sidepanel')
            <div id="right-panel" class="right-panel">
            @include('includes.navbar')
            @yield('content')
            </div>
        @endguest
    @include('includes.footer')
</body>
</html>
