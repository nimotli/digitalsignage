<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.header')
</head>
<body class="bg-dark">
    @yield('content')
    @include('includes.footer')
</body>
</html>
