@extends('layouts.app')

@section('content')
    <div class="container bg-light my-5 py-3 border" >
        <h2>Supperposition</h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('SequenceController@changeOverlay',$id)}}">
            @csrf
            <br>
            <div class=" mx-auto">
                <label for="ads">Element de la supperposition</label>
                <select name="overlay" class="form-control">
                    @foreach($ads as $ad)
                        <option @if($sequence->overlay==$ad->id)selected @endif value="{{$ad->id}}">{{$ad->name}}</option>
                    @endforeach
                </select>
            </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Valider</button>
        </form>
    </div>
@endsection