@extends('layouts.app')

@section('content')

<div class="content mt-3">
    @if (\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Table</strong>
                        <a href="{{ route('sequences.create') }}" class="float-right btn btn-primary">Creer sequence</a>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nom sequence</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sequences as $sequence)
                                <tr> 
                                    <td>{{$sequence['id']}}</td>
                                    <td>{{$sequence['nom']}}</td>
                                    <td><a href="{{action('SequenceController@overlay', $sequence['id'])}}" class="btn btn-dark">Superposer</a></td>
                                    <td><a href="{{action('SequenceController@edit', $sequence['id'])}}" class="btn btn-warning">Modifier</a></td>
                                    <td>
                                    <form action="{{action('SequenceController@destroy', $sequence['id'])}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger" type="submit">Supprimer</button>
                                    </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>   

@endsection