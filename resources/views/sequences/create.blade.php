@extends('layouts.app')

@section('content')

<div class="container ">
    <h2>Creer une sequence</h2><br/>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div id="restEl">
    <div class="row p-4 my-3 bg-light border" style="height:300px; overflow-y:scroll;">
        @foreach($multimedias as $mult)
            @if($mult->uploader==$id || $mult->type=="public")
                <div class="col-md-2">
                    <div class="card text-center">
                        <div class="card-header text-muted">
                                {{$mult->media_type}} | {{$mult->type}}
                        </div>
                        
                        <div class="card-body" >
                            @if($mult->media_type=='Video')
                                <video src="{{ asset('multimedia/'.$mult->filename)}}" class="embed-responsive embed-responsive-16by9" id="{{$mult->id}}" draggable="true" ondragstart="drag(event)"></video>
                            @else
                            <img src="{{ asset('multimedia/'.$mult->filename)}}" class="multImages" id="{{$mult->id}}" draggable="true" ondragstart="drag(event)">
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
    <div id="seqErrors" class="alert alert-danger" style="display:none;">
            <p id="error">please fill all sequence boxes or delete the empty ones</p>
    </div>
    <a class="float-right py-5" href="#" onclick="createDropzone(event)"> <i class="menu-icon fa fa-plus m-2" style="font-size:2.5em;"></i></a>
    <div id="parent" class="row p-4 text-center flex-nowrap bg-light" style="overflow-x:scroll;">
            <div class="col-md-2 border border-primary draggable" style="height:100px;background-size: contain;display:block;"
             ondrop="drop(event,this)" ondragover="allowDrop(event)">
             <p style="display: none;">test</p>
             <div style="position: absolute;top:0px;left:0px;bottom:0px;right:0px;">   
                    <video style="width:100%; height:100%;z-index:-1;"></video>
                   </div>
             <div style="position:relative;top:85%;">
                    <input type="text" class="form-control" placeholder="Duree" readonly>
                </div>
            
            
            </div>
    </div>
</div>
    <form method="post" action="{{url('sequences')}}" class="my-3 bg-light" id="myform">
        @csrf
        <div class="form-group">
            <label for="nom">Nom</label>
            <input type="nom" class="form-control" id="nom" name="nom" placeholder="Nom sequence" required>
        </div>
        <input type="hidden" name="sequenceInfo" id="sequenceInfo" required>
        <button type="button" onclick="send(event)" class="btn btn-primary mb-2">Creer</button>
    </form>
</div>

<!--using it as a prefab to clone-->
<div id="original" class="col-md-2 border border-primary draggable" style="height:100px;background-size: contain;display:none;"
        ondrop="drop(event,this)" ondragover="allowDrop(event)">
        <div style="z-index:10;position:absolute;right:5%;">
            <a class="float-right" href="#" onclick="deleteDropzone(this,event)"><i class="menu-icon fa fa-times"></i></a>
        </div>
        <p style="display: none;">test</p>
        <div style="position: absolute;top:0px;left:0px;bottom:0px;right:0px;">   
            <video style="width:100%; height:100%;z-index:1;"></video>
        </div>
        <div style="position:relative;top:85%;">
            <input type="text" class="form-control" placeholder="Duree" readonly>
        </div>
    </div>  
<!--end prefab--> 

<script>
    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData("text", ev.target.id);
    }

    function drop(ev,target) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        target.getElementsByTagName("video")[0].src="";
        target.style.backgroundImage="";
        
        var elem=document.getElementById(data);
        if(elem.tagName=="IMG")
        {
            target.style.backgroundImage="url("+document.getElementById(data).src+")";
            target.getElementsByTagName("input")[0].value=4;
            target.getElementsByTagName("input")[0].readOnly = false; 
        }
        else
        {            
            target.getElementsByTagName("video")[0].setAttribute('src', document.getElementById(data).src);
            target.getElementsByTagName("input")[0].value=document.getElementById(data).duration;
            target.getElementsByTagName("input")[0].readOnly = true; 
        }
        target.getElementsByTagName("p")[0].innerHTML=data;
    }
    function createDropzone(ev)
    {
        ev.preventDefault();
        duplicate();
    }
    function deleteDropzone(item,ev)
    {
        ev.preventDefault();
        item.parentNode.parentNode.parentNode.removeChild(item.parentNode.parentNode);
    }
    function duplicate() 
    {
        var original = document.getElementById('original');
        var theParent=document.getElementById('parent')
        var clone = original.cloneNode(true); // "deep" clone
        clone.id="";
        clone.style.display="block";
        theParent.appendChild(clone);
    }
    function formSequenceInfo()
    {
        var sequenceInfo="";
        var parentElem=document.getElementById('parent');
        for(i=0 ;i<parentElem.children.length;i++)
        {
            if(parentElem.children[i].getElementsByTagName("p")[0].innerHTML=='test')
            {
                return 'null';
            }
            else
            {
                var duree=4;
                if(parentElem.children[i].getElementsByTagName("input")[0].value!="")
                {
                    duree=parentElem.children[i].getElementsByTagName("input")[0].value;
                }
                if(i!=parentElem.children.length-1)
                    sequenceInfo+=i+":"+parentElem.children[i].getElementsByTagName("p")[0].innerHTML+":"+duree+"-";
                else
                    sequenceInfo+=i+":"+parentElem.children[i].getElementsByTagName("p")[0].innerHTML+":"+duree;
            }
            
        }
        return sequenceInfo;
    }
    function send(ev)
    {
        ev.preventDefault();
        var form=document.getElementById('myform');
        var sequenceInfo=formSequenceInfo();
        if(sequenceInfo!='null')
        {
            var elem=document.getElementById('seqErrors');
            elem.style.display="none";
            document.getElementById('sequenceInfo').value=sequenceInfo;
            form.submit();
        }
        else
        {
            var elem=document.getElementById('seqErrors');
            elem.style.display="block";
        }
        
    }
</script>

@endsection