@extends('layouts.app')

@section('content')
<style>
  /* calendar */
table.calendar		{ border-left:1px solid #999;width:100%; }
tr.calendar-row	{  }
td.calendar-day	{ min-height:80px; font-size:11px; position:relative; } * html div.calendar-day { height:80px; }
td.calendar-day:hover	{ border:2px dashed black; }
td.calendar-day-np	{ background:#eee; min-height:80px; } * html div.calendar-day-np { height:80px; }
td.calendar-day-head { background:#ccc; font-weight:bold; text-align:center; width:120px; padding:5px; border-bottom:1px solid #999; border-top:1px solid #999; border-right:1px solid #999; }
div.day-number		{ background:#999; padding:5px; color:#fff; font-weight:bold; width:20px; text-align:center; }
/* shared */
td.calendar-day, td.calendar-day-np { width:120px; padding:5px; border-bottom:1px solid #999; border-right:1px solid #999; }
</style>
<div class="container"> 
        <!--Month view-->
        <div id="monthDiv" class="row my-3 p-3 bg-light" >
            <div class="col-12">
                <center><div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-secondary" onclick="previous()"><i class="fa fa-backward"></i> Previous month</button>
                    <button type="button" class="btn btn-secondary" onclick="next()">Next month <i class="fa fa-forward"></i></button>
                </div></center>
            </div>
            <div class="col-12" id="calHolder">
                {!! $content !!}
            </div>
            
                <form id="scheduleFrom" action="{{action('ScheduleController@makeSchedule',$id)}}" method="post" class="pt-3 mx-auto">
                @csrf
                <input type="hidden" name="id">
                <input type="hidden" name="dataString" id="dataString">
                <button class="btn btn-primary px-3" onclick="send(event)">Enregistrer</button>
            </form>
        </div>
</div>
<div id="dayDivOriginal" style="display:none;" class="container my-3 p-3 bg-light dayClass">
    <h3 class="display-6 my-3 text-center"></h3>
    <div class="row m-1 border timeRow">
        <div class="col-1 p-2 border border-dark text-center" >
            All day
        </div>
        <div class="col-1 ">
            <button class="btn btn-primary p-2 addSequence" onclick="callit(this)" data-toggle="modal" data-target=".bd-example-modal-lg">
                <i class="fa fa-plus"></i>
            </button> 
        </div>
    </div>
    @for($i=0;$i<48;$i++)
        @if($i%2==0)
            <div class="row m-1  border timeRow">
                <div class="col-1 p-2 border border-dark text-center" >
                    {{floor($i/2)}}:00
                </div>
                <div class="col-1 ">
                    <button class="btn btn-primary p-2 addSequence" onclick="callit(this)"  data-toggle="modal" data-target=".bd-example-modal-lg">
                        <i class="fa fa-plus"></i>
                    </button> 
                </div>
            </div>
        @else
            <div class="row m-1  border timeRow">
                <div class="col-1 p-2 border border-dark text-center" >
                    {{floor($i/2)}}:30
                </div>
                <div class="col-1 ">
                    <button class="btn btn-primary p-2 addSequence" onclick="callit(this)" data-toggle="modal" data-target=".bd-example-modal-lg">
                        <i class="fa fa-plus"></i>
                    </button> 
                </div>
            </div>
        @endif
    @endfor
</div>
<div id="addingPosition"></div>
</div>

<div id="original" class="col pt-1 text-center sequenceClass" style="display:none;">
    <a href="#" onclick="remove(event,this)"><i class="fa fa-times float-right"></i></a>
    <p style="color:white;"></p>
    <p style="display:none;"></p>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content p-3">
        <div class="row">
            <h5 class="col-12 my-3">Sequences</h5>
            @foreach($sequences as $sequence)
                <button class="col-md-3 m-2 px-2 py-1 btn btn-warning seqBtn"  id="{{$sequence->id}}">{{$sequence->nom}}</button>
            @endforeach
        </div>
    </div>
</div>
</div>

 
<script>
    var lastChanged;
    var set=false;
    var clickedId;
    var defaultBorder;
    var lastOpenedDay=null;
    var currentCal=getCurrentCal();

    function getCurrentCal()
    {
        var cals = document.getElementsByClassName('calendar');
        for(i=0;i<cals.length;i++)
        {
            if($(cals[i]).is(":visible"))
            {
                return cals[i].id;
            }
        }
    }

    function next()
    {
        var date=currentCal.split('-');
        var month=date[0];
        var year=date[1];
        if(month<12)
            month++;
        else
        {
            month=1;
            year++;
        }
        var newElem=document.getElementById(month+"-"+year);
        if(newElem==null)
        {
            $.ajax({
                type:'POST',
                url:'/schedules/getNewCal',
                data:{
                "_token": "{{ csrf_token() }}",
                "month": month,
                "year": year,
                    },
                success:function(data){
                    $('.calDiv').hide();
                    $("#calHolder").append(data);
                    currentCal=month+"-"+year;
                },
                error: function(xhr, textStatus, errorThrown){
                    alert('request failed');
                }
            });
        }
        else
        {
            $('.calDiv').hide();
            newElem.parentElement.style.display="block";
            currentCal=month+"-"+year;
        }
    }

    function previous()
    {
        var date=currentCal.split('-');
        var month=date[0];
        var year=date[1];
        if(month>1)
            month--;
        else
        {
            month=12;
            year--;
        }
        var newElem=document.getElementById(month+"-"+year);
        if(newElem==null)
        {
            $.ajax({
            type:'POST',
            url:'/schedules/getNewCal',
            data:{
            "_token": "{{ csrf_token() }}",
            "month": month,
            "year": year,
                },
            success:function(data){
                $('.calDiv').hide();
                $("#calHolder").append(data);
                currentCal=month+"-"+year;
            },
            error: function(xhr, textStatus, errorThrown){
                alert('request failed');
            }
            });
        }
        else
        {
            $('.calDiv').hide();
            $(newElem).parent().show();
            currentCal=month+"-"+year;
        }
        
    }

    $('.seqBtn').on('click',function(){
        addSequence(clickedId,$(this).attr('id'),$(this).text());
    })

    function callit(target){
        //addSequence($(this).parent().attr('id'));
        clickedId=target.parentElement;
    }

    $(document).on('click','.calendar-day','p.test',function(){
        if(set)
        {
            lastChanged.style.border=defaultBorder;
        }
        var clickedDate=$(this).attr('id');   
        lastChanged=document.getElementById(clickedDate);
        defaultBorder=$(this).css("border");
        $(this).find('div').addClass('bg-warning');
        $(this).css("border","2px dotted black")
        
        if($('#day'+clickedDate).length>0)
        {    
            $(lastOpenedDay).fadeOut(300,function(){
                $('#day'+clickedDate).find('h3').text(clickedDate);
                $('#day'+clickedDate).fadeIn(500);
            });
            lastOpenedDay=document.getElementById('day'+clickedDate);
        }
        else
        {
            var original=document.getElementById('dayDivOriginal');
            var clone = original.cloneNode(true);
            clone.id='day'+clickedDate;
            //clone.style.display="block";
            document.getElementById('addingPosition').appendChild(clone);
            
            if(lastOpenedDay!=null)
            {
                $(lastOpenedDay).fadeOut(300,function(){
                    $(clone).find('h3').text(clickedDate);
                    $(clone).fadeIn(500);
                })
            }
            else
            {
                $(clone).find('h3').text(clickedDate);
                $(clone).fadeIn(500);
            }
            lastOpenedDay=document.getElementById('day'+clickedDate);
        }

        if(set==false)
            set=true;
    });

    function addSequence(id,seqId,seqName)
    {
        var timeRows=document.getElementsByClassName('timeRow');
        var original=document.getElementById('original');
        var clone = original.cloneNode(true);
        clone.id="";
        clone.style.display="block";
        var r=Math.floor(Math.random() * 256);
        var g=Math.floor(Math.random() * 256);
        var b=Math.floor(Math.random() * 256);
        clone.style.backgroundColor="rgb("+r+","+g+","+b+")";
        clone.getElementsByTagName('p')[0].innerHTML=seqName;
        clone.getElementsByTagName('p')[1].innerHTML=seqId;
        id.parentElement.insertBefore(clone,id);
    }
    function remove(ev,element)
    {
        ev.preventDefault();
        element.parentNode.parentNode.removeChild(element.parentNode);
    }

    function generateData()
    {
        var days=document.getElementsByClassName('dayClass');
        var dataString=""
        var dateSet=false;
        var houreSet=false
        for(i=0;i<days.length;i++)
        {
            var houres=days[i].getElementsByClassName('timeRow');
            for(j=0;j<houres.length;j++)
            {
                var sequences=houres[j].getElementsByClassName('sequenceClass');
                if(sequences.length>0)
                {
                    for(k=0;k<sequences.length;k++)
                    {
                        if(dateSet==false)
                        {
                            dataString+=days[i].getElementsByTagName('h3')[0].innerHTML+"!";
                            dateSet=true;
                        }
                        if(houreSet==false)
                        {
                            dataString+=j+':';
                            houreSet=true;
                        }
                        if(k<sequences.length-1)
                            dataString+=sequences[k].getElementsByTagName('p')[1].innerHTML+',';
                        else
                            dataString+=sequences[k].getElementsByTagName('p')[1].innerHTML;
                    }
                }
                houreSet=false;
                if(sequences.length>0 && j<houres.length-1)
                    dataString+='-';
            }
            dateSet=false;
            if(i<days.length-1 )
                dataString+='|';
        }
        return dataString;
    }

    function send(ev)
    {
        ev.preventDefault();

        var dataString=generateData();
        document.getElementById("dataString").value=dataString;

        $('#scheduleFrom').submit();
    }
</script>
@endsection
