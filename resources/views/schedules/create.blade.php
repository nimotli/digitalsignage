@extends('layouts.app')

@section('content')
    <div class="container bg-light my-5 py-3" >
        <h2>Creation des plannings</h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{url('schedules')}}">
            @csrf
            {{csrf_field()}}
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" >
                </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Creer</button>
        </form>
    </div>
@endsection