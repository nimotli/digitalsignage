@extends('layouts.app')

@section('content')
    <div class="container bg-light my-5 py-3" >
        <h2>Modification du planning</h2><br/>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('ScheduleController@update',$id)}}">
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" name="nom" value="{{$schedule->name}}" >
                </div>
            <br>
            <button type="submit" class="btn btn-primary mb-2">Modifier</button>
        </form>
    </div>
@endsection