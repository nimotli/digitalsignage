@extends('layouts.app')

@section('content')
<div class="content mt-3">
    @if (\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Data Table</strong>
                        <a href="{{ route('schedules.create') }}" class="float-right btn btn-primary">Creer planning</a>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered dt-responsive responive">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nom</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($schedules as $schedule)
                                <tr>
                                    <td>{{$schedule['id']}}</td>
                                    <td>{{$schedule['name']}}</td>
                                    <?php $found =false; ?>
                                    @foreach($sp as $scheP)
                                        @if($scheP->schedule==$schedule->id)
                                            <?php $found =true; ?>
                                            @break
                                        @endif
                                    @endforeach
                                    @if($found)
                                        <td>
                                            <a href="{{action('ScheduleController@editSchedule', $schedule['id'])}}" class="btn btn-secondary">Modifier planning</a>
                                        </td>
                                    @else
                                        <td>
                                            <a href="{{action('ScheduleController@createSchedule', $schedule['id'])}}" class="btn btn-success">Planner</a>
                                        </td>
                                    @endif
                                    
                                    <td>
                                        <a href="{{action('ScheduleController@edit', $schedule['id'])}}" class="btn btn-warning">Modifier</a>
                                    </td>
                                    <td>
                                        <form action="{{action('ScheduleController@destroy', $schedule['id'])}}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button class="btn btn-danger" type="submit">Supprimer</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection